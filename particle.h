#pragma once
#include "gr.h"

typedef void (*TempFunc)(double temp, SDL_Color *c);
//x(t+dt) = x(t)+dt*v(t)
//vx(t+dt) = vx(t)*(1-fx*dt) +dt*a(t)
//when t <=0.0 particle dies
typedef struct{
	double x;
	double y;
	double t;
	int z;
	double vx;
	double vy;
	double vt;
	double ax;
	double ay;
	double at;
	double fx;
	double fy;
	double ft;
	Sprite *s;
	TempFunc tf;
}Particle;

void SetParticleBoundingBox(double x, double y, double xx, double yy); // x,y - coords; xx,yy -- width, height; THIS FUNCTION SHOULD BE CALLED BEFORE USING PARTICLES
void AddParticle(Particle *p); //doesn't copy p, p will be freed automagically when it is not needed anymore
void UpdateAndQueueDrawParticles(double dt);
