#include "geometry.h"
#include "util.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

void NormV(Vect *v){
	double length;

	length = v->x*v->x+v->y*v->y;
	length = sqrt(length);
	v->x /= length;
	v->y /= length;
}

void PrintVect(Vect *a){
	printf("[%lf, %lf]", a->x, a->y);
}

double DotProduct(Vect *a, Vect *b){
	return a->x*b->x+a->y*b->y;
}


int InPoly(Poly *p, Vect *v){	
	int i;

	for(i=0; i<p->novec; i++){
		if( DotProduct(v, &p->n[i]) >= p->a[i]) return false;
	}

	return true;
}


Poly *NewPoly(){
	Poly *p;

	p=malloc(sizeof(Poly));
	p->novec=p->alloc=0;
	p->a=0;
	p->n=0;
	p->v=0;

	return p;
};

int AddToPoly(Poly *p,Vect *v){
	if( p->novec > (p->alloc-1)){
		if(p->alloc <1)
			p->alloc=1;
		Vect *tn;
		double *ta;
		Vect *tv;
		tn = realloc(p->n, sizeof(Vect)*p->alloc*2);
		tv = realloc(p->v, sizeof(Vect)*p->alloc*2);
		if(tn==0 || tv == 0)
			return false;

		p->n = tn;
		p->v = tv;

		ta = realloc(p->a, sizeof(double)*p->alloc*2);
		if(ta == 0)
			return false;
		p->a = ta;

		p->alloc *= 2;
	}

	p->v[p->novec].x = v->x;
	p->v[p->novec].y = v->y;

	if(p->novec){
		//compute normal between actual and preceding
		p->n[p->novec-1].x = -p->v[p->novec-1].y + p->v[p->novec].y;
		p->n[p->novec-1].y = +p->v[p->novec-1].x - p->v[p->novec].x;
		NormV(&p->n[p->novec-1]);
		p->a[p->novec-1] = p->n[p->novec-1].x*v->x + p->n[p->novec-1].y*v->y;

		//compute normal between actual and first
		p->n[p->novec].x = -p->v[p->novec].y + p->v[0].y;
		p->n[p->novec].y = +p->v[p->novec].x - p->v[0].x;
		NormV(&p->n[p->novec]);
		p->a[p->novec] = p->n[p->novec].x*v->x + p->n[p->novec].y*v->y;

	}
	p->novec++;
	return true;
}

int AddToPolyXY(Poly *p, double x, double y){
	Vect v;
	v.x=x;
	v.y=y;
	return AddToPoly(p, &v);
}

void FreePoly(Poly *p){
	if(p==0)return;
	free(p->v);
	p->v=0;
	free(p->n);
	p->n=0;
	free(p->a);
	p->a=0;
	free(p);
}

Poly *NewRect(double x, double y, double xx, double yy){
	Poly *p;

	p=NewPoly();
	AddToPolyXY(p, x, y);
	AddToPolyXY(p, x+xx, y);
	AddToPolyXY(p, x+xx, y+yy);
	AddToPolyXY(p, x, y+yy);

	return p;
}

int PolyInPoly(Poly *p1, Poly *p2){
	int cur1;
	int cur2;
	
	//we check if any vertex of p1 is in p2
	for(cur1=0; cur1 < p1->novec; cur1++){
		if(InPoly(p2, &p1->v[cur1])){
			return true;
		}
	}

	//we check if any vertex of p1 is in p2
	for(cur2=0; cur2 < p2->novec; cur2++){
		if(InPoly(p1, &p2->v[cur2])){
			return true;
		}
	}

	return false;
}

int PolyInPolyComp(Poly *p1, Poly *p2){
	int cur1;
	int in;
	int cur2;
	
	//we check if every vertex of p1 is in p2
	in = true;
	for(cur1=0; cur1 < p1->novec; cur1++){
		if(!InPoly(p2, &p1->v[cur1])){
			in = false;
		}
	}
	if(in)
		return 1;

	//we check if any vertex of p1 is in p2
	in = true;
	for(cur2=0; cur2 < p2->novec; cur2++){
		if(!InPoly(p1, &p2->v[cur2])){
			in = false;
		}
	}
	if(in)
		return 2;

	return 0;
}

Poly *TranslatePoly(Poly *p, Vect *v){
	return TranslatePolyXY(p, v->x, v->y);
}

Poly *TranslatePolyXY(Poly *p, double x, double y){
	Poly *r;
	int i;

	r=NewPoly();
	for(i=0; i< p->novec; i++){
		AddToPolyXY(r, p->v[i].x+x, p->v[i].y+y);
	}
	return r;
}

void MultiplyPoly(Poly *p, double xm, double ym){
	int i;
	for(i=0; i< p->novec; i++){
		p->v[i].x *= xm;
		p->v[i].y *= ym;
	}
} 


void PrintPoly(Poly *p){
	int i;

	printf("NoV: %d\n", p->novec);
	for(i=0; i<p->novec; i++)
		printf("[%lf, %lf] [%lf, %lf] %lf \n", p->v[i].x, p->v[i].y, p->n[i].x, p->n[i].y, p->a[i]);
}

/* format for saving poly into string: 
 * space delimited values in ascii:
 * novec x y x y x y ...
 * */
#define P2SBUFSIZE 64
char *Poly2String(Poly *p){
	int i;
	char *buf;
	char *chain;
	int allocated;

	buf = malloc(P2SBUFSIZE*sizeof(char));
	chain = 0;

	snprintf(buf, P2SBUFSIZE, "%d ", p->novec);
	allocated = strlen(buf)+2;
	chain = malloc(allocated*sizeof(char));
	strcpy(chain, buf);

	for(i=0; i<p->novec; i++){
		snprintf(buf, P2SBUFSIZE, "%lf %lf ", p->v[i].x, p->v[i].y);
		allocated += strlen(buf);
		chain = realloc(chain, allocated*sizeof(char)); //TODO: check return value
		sprintf(&chain[strlen(chain)],"%s", buf);
	}

	free(buf);
	return chain;
}

void FreePoly2String(char *s){
	free(s);
}

Poly *String2Poly(char *s){
//TODO: return values check, use strtok
	char *cur;
	Poly *p;
	int novec, i;
	double x,y;

	p = NewPoly();
	sscanf(s, "%d", &novec);
	cur = s;

	for(i=0; i<novec; i++){
		cur = strchr(&cur[1], ' ');
		sscanf(cur, "%lf", &x);
		cur = strchr(&cur[1], ' ');
		sscanf(cur, "%lf", &y);
		AddToPolyXY(p, x, y);
	}

	return p;
}


Area *NewArea(){
	Area *a;

	a = malloc(sizeof(Area));
	a->nopol = 0;
	a->p = 0;

	return a;
}

void FreeArea(Area *a){
	int i;
	
	if(a == 0)
		return;

	for(i=0; i< a->nopol; i++){
		FreePoly(a->p[i]);
	}

	a->nopol = 0;
	free(a->p);
	a->p = 0;
	free(a);
}

int AddToArea(Area *a, Poly *p){
//TODO: check for return values
	a->nopol += 1;
	a->p = realloc(a->p, sizeof(Poly *)*a->nopol);
	a->p[a->nopol-1] = TranslatePolyXY(p, 0, 0);

	return true;
}

int InArea(Area *a, Vect *v){
	int i;
	for(i=0; i<a->nopol; i++){
		if(InPoly(a->p[i], v))
			return true;
	}
	return false;
}
	
int AreaInArea(Area *a1, Area *a2){
	int i,j;

	if(a1 == 0 || a2==0)
		return false;

	for(i=0; i<a1->nopol; i++)
		for(j=0; j<a2->nopol; j++)
			if(PolyInPoly(a1->p[i], a2->p[j]))
				return true;
	return false;
}

int AreaInAreaComp(Area *a1, Area *a2){
	//TODO: rewrite to correct behavior this is only sufficient behavior to current use in elilanci but not correct
	int i,j;
	int aia;

	if(a1 == 0 || a2==0)
		return false;

	aia=true;
	for(i=0; i<a1->nopol; i++)
		for(j=0; j<a2->nopol; j++){
			if(!PolyInPolyComp(a1->p[i], a2->p[j]))
				aia =false;
		}
	return aia;
}

Area *TranslateAreaXY(Area *a, double x, double y){
	Area *new;
	Poly *p;
	int i;
	
	new = NewArea();
	for(i=0; i< a->nopol; i++){
		p = TranslatePolyXY(a->p[i], x, y);
		AddToArea(new, p);
		FreePoly(p);
	}

	return new;
}

Area *TranslateArea(Area *a, Vect *v){
	return TranslateAreaXY(a, v->x, v->y);
}

void PrintArea(Area *a){
	//TODO:dopsat;
}

char *Area2String(Area *a){
	int i,al;
	char *buf, *chain;
	chain = 0;
	buf = malloc(sizeof(char)* P2SBUFSIZE);
	snprintf(buf, P2SBUFSIZE, "%d\n", a->nopol);
	chain = strdup(buf);
	al = strlen(chain)+1;
	free(buf);
	for(i=0; i<a->nopol; i++){
		buf = Poly2String(a->p[i]);
		al += strlen(buf)+2; //+2 for \n
		chain = realloc(chain, al);
		sprintf(&chain[strlen(chain)], "%s\n", buf);
		FreePoly2String(buf);
	}
	return chain;
}

void FreeArea2String(char *s){
	free(s);
}
 
Area *String2Area(char *s){
//TODO: return values check
	char *cur;
	Poly *tp;
	Area *a;
	int nopol, i;

	a = NewArea();
	sscanf(s, "%d", &nopol);
	cur = s;

	for(i=0; i<nopol; i++){
		cur = strstr(&cur[strlen("\n")], "\n"); //BUG: if strlen("\n")>1 && strlen(nopol)==1
		tp = String2Poly(cur);
		AddToArea(a, tp);
		FreePoly(tp);
	}

	return a;
}

