#pragma once

#define CONFIG_FILENAME "config.glb"

typedef struct{
	unsigned int screen_width;
	unsigned int screen_height;
	unsigned int fullscreen;
	unsigned int texture_lod;

	unsigned int pl1_key_up;
	unsigned int pl1_key_down;
	unsigned int pl1_key_left;
	unsigned int pl1_key_right;
	unsigned int pl1_key_fire;
	unsigned int pl1_key_suicide;
	unsigned int pl1_key_switch;

	unsigned int pl2_key_up;
	unsigned int pl2_key_down;
	unsigned int pl2_key_left;
	unsigned int pl2_key_right;
	unsigned int pl2_key_fire;
	unsigned int pl2_key_suicide;
	unsigned int pl2_key_switch;
}Config;

int LoadConfig();
int SaveConfig();
Config GetConfig();
extern void SetConfig(const Config c);
