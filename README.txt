This is basically a game strongly inspired by the game called Bulanci.
Graphics is SVG based rendered on load time by cairo and then drawn using
OpenGl.
Collisions are based on convex polygons.
It is rather proof of concept than a real game and so far one man show.

It's licensed under GPL.

It uses cmake 2.4 for managing the build process.

Build Requirements:
	development versions of folowing libs:
	sdl, sdl-mixer
	cairo
	librsvg-2
	opengl - probably mesa

Debian Build Requirements:
	libsdl-ttf2.0-dev
	libsdl-mixer1.2-dev
	libsdl-gfx1.2-dev
	libsdl-sound1.2-dev
	libcairo2-dev
	librsvg2-dev

Compilation and run withouth install:
	mkdir build
	cd build
	cmake .. -DINSTALLLESS='..'
	make
	./elilanci

Compilation for install into /tmp/lilanci
	mkdir build
	cd build
	cmake .. -DCMAKE_INSTALL_PREFIX=/tmp/lilanci
	make
	make install
