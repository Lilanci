#include "config.h"
#include "gr.h"
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <sys/types.h>
#include <cairo.h>
#include <librsvg/rsvg.h>
#include <librsvg/rsvg-cairo.h>
#include "font.h"


#define TRECT(x,y,w,h, tw, th)	glBegin(GL_TRIANGLE_STRIP);glTexCoord2f(0.0,h/th);glVertex2f(x, y+1*h);glTexCoord2f(0.0,0.0);glVertex2f(x, y);glTexCoord2f(w/tw,h/th);glVertex2f(x+1*w, y+1*h);glTexCoord2f(w/tw,0.0);glVertex2f(x+1*w, y);glEnd();
#define RECT(x,y,w,h)	glBegin(GL_TRIANGLE_STRIP);glVertex2f(x, y+1*h);glVertex2f(x, y);glVertex2f(x+1*w, y+1*h);glVertex2f(x+1*w, y);glEnd();

#define TTRECT(x,y,w,h,sw,sh, tw, th) glBegin(GL_TRIANGLE_STRIP);glTexCoord2f(0.0,sh/th);glVertex2f(x, y+1*h);glTexCoord2f(0.0,0.0);glVertex2f(x, y);glTexCoord2f(sw/tw,sh/th);glVertex2f(x+1*w, y+1*h);glTexCoord2f(sw/tw,0.0);glVertex2f(x+1*w, y);glEnd();

unsigned int occupied=0;

typedef struct{
	GLuint *surfaces;
	unsigned int allocated;
}SurfaceList;

SurfaceList gsprite_surfaces;
int AddSurface(GLuint s);
void RemoveSurface(GLuint s);
void RemoveSID(int SID);
void FlushSpriteSurfaces();
int SetVideoMode(int width, int height);
SDL_Surface *gscreen;

typedef struct{
	Sprite **sprites;
	unsigned int allocated;
}SpriteList;

SpriteList gsprites;
int AddSprite(Sprite *s);
void RemoveSprite(Sprite *s);

typedef struct{
	Sprite *sprite;
	double x;
	double y;
	double xx;
	double yy;
	int z;			//z
	SDL_Color c;
}SpriteQueueItem;

typedef struct{
	SpriteQueueItem *items;
	unsigned int allocated;
	unsigned int next_id;
}SpriteQueue;
SpriteQueue gsprite_queue;
SpriteQueue gsprite_queue_sorted;
void FlushSpriteQueue();
void RotateSurface(int SID, int rotation);

int ginvalidsprites;

GLuint load_svg (char *file, int width, int height, double *pdw, double *pdh);

int G2SX(double x);
int G2SY(double y);

int G2SX(double x){
	return (x/(double)SCREEN_WIDTH)*GetConfig().screen_width;
}
int G2SY(double y){
	return (y/(double)SCREEN_HEIGHT)*GetConfig().screen_height;
}
double S2GX(int x){
	return (x*(double)SCREEN_WIDTH)/(double)GetConfig().screen_width;
}
double S2GY(int y){
	return (y*(double)SCREEN_HEIGHT)/(double)GetConfig().screen_height;
}

int SetVideoMode(int width, int height){
	SDL_Rect **modes;
	int ibest;
	int bestdif;
	int i;	
	Uint32 flags;

	Config config=GetConfig();

	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	flags = SDL_OPENGL ;
	if(config.fullscreen)
		flags = flags |SDL_FULLSCREEN;
	/* Get available fullscreen/hardware modes */
	modes=SDL_ListModes(NULL, flags);

	/* Check if there are any modes available */
	if(modes == (SDL_Rect **)0){
		printf("No modes available!\n");
		exit(-1);
	}

	/* Check if our resolution is restricted */
	if(modes == (SDL_Rect **)-1){
		printf("All resolutions available.\n");
		gscreen = SDL_SetVideoMode(config.screen_width, config.screen_height, 0, flags);
	}
	else{
		/* Print valid modes */
		printf("Available Modes\n");
		bestdif = abs((modes[0]->w - config.screen_width) + (modes[0]->h - config.screen_height));
		ibest = 0;
		for(i=0;modes[i];++i){
			printf("  %d x %d\n", modes[i]->w, modes[i]->h);
			if(bestdif > abs((modes[i]->w - config.screen_width) + (modes[i]->h - config.screen_height))){
				bestdif = abs((modes[i]->w - config.screen_width) + (modes[i]->h - config.screen_height));
				ibest = i;
			}
		}
		printf("Using:  %d x %d\n", modes[ibest]->w, modes[ibest]->h);
		gscreen = SDL_SetVideoMode(modes[ibest]->w, modes[ibest]->h, 0, flags);
		config.screen_height = modes[ibest]->h;
		config.screen_width = modes[ibest]->w;
		SetConfig(config);
	}
	return 0;
}

int AddSurface(GLuint s){ //returns -1 on error, SID if success
	int i;
	GLuint *tmp;
	for(i=0;i<gsprite_surfaces.allocated;i++)
		if( gsprite_surfaces.surfaces[i]==s ){
			return i;
		}
	
	for(i=0;i<gsprite_surfaces.allocated;i++)
		if( gsprite_surfaces.surfaces[i]==0 ){
			gsprite_surfaces.surfaces[i]=s;
			return i;
		}
	i=gsprite_surfaces.allocated;
	tmp = (GLuint*) realloc(gsprite_surfaces.surfaces, sizeof(GLuint)*i*2); //if the allocated space is not wide enough, then allocate double space than previous
	if(tmp==0)
		return -1;
	gsprite_surfaces.surfaces=tmp;
	memset(&gsprite_surfaces.surfaces[i],0, sizeof(GLuint)*i); //zero newly allocated space
	gsprite_surfaces.allocated=i*2;
	gsprite_surfaces.surfaces[i]=s;
	return i;
}

void RemoveSurface(GLuint s){
	int i,iwannakillyoubitch;

	iwannakillyoubitch=1;
	for(i=0;i<gsprite_surfaces.allocated;i++){
		if( gsprite_surfaces.surfaces[i]==s ){
			gsprite_surfaces.surfaces[i]=0;
		}
		if( i>=gsprite_surfaces.allocated/2 && iwannakillyoubitch && gsprite_surfaces.surfaces[i]!=0 )
			iwannakillyoubitch=0;
	}

	if(iwannakillyoubitch){
		gsprite_surfaces.allocated /=2;
		gsprite_surfaces.surfaces = (GLuint *)realloc(gsprite_surfaces.surfaces, sizeof(GLuint)*gsprite_surfaces.allocated);//TODO:check return value
	}
}

void RemoveSID(int SID){
	int i,iwannakillyoubitch;

	iwannakillyoubitch=1;
	for(i=0;i<gsprite_surfaces.allocated;i++){
		if( i==SID ){
			gsprite_surfaces.surfaces[i]=0;
		}
		if( i>=gsprite_surfaces.allocated/2 && iwannakillyoubitch && gsprite_surfaces.surfaces[i]!=0 )
			iwannakillyoubitch=0;
	}

	if(iwannakillyoubitch){
		gsprite_surfaces.allocated /=2;
		gsprite_surfaces.surfaces = (GLuint *)realloc(gsprite_surfaces.surfaces, sizeof(GLuint)*gsprite_surfaces.allocated);//TODO:check return value
	}
}

int AddSprite(Sprite *s){//returns 0 on error, Sprite ID on success
	int i;
	Sprite **tmp;
	for(i=0;i<gsprites.allocated;i++)
		if( gsprites.sprites[i]==s ){
			return i;
		}
	
	for(i=0;i<gsprites.allocated;i++)
		if( gsprites.sprites[i]==0 ){
			gsprites.sprites[i]=s;
			return i;
		}
	i=gsprites.allocated;
	tmp=(Sprite **)realloc(gsprites.sprites, sizeof(Sprite *)*i*2); //if the allocated space is not wide enough, then allocate double space than previous
	if(tmp==0)
		return 0;
	gsprites.sprites=tmp;
	memset(&gsprites.sprites[i],0, sizeof(Sprite *)*i); //zero newly allocated space
	gsprites.allocated=i*2;
	gsprites.sprites[i]=s;
	return i;
}

void RemoveSprite(Sprite *s){
	int i,iwannakillyoubitch;

	iwannakillyoubitch=1;
	for(i=0;i<gsprites.allocated;i++){
		if( gsprites.sprites[i]==s ){
			gsprites.sprites[i]=0;
		}
		if( i>=gsprites.allocated/2 && iwannakillyoubitch && gsprites.sprites[i]!=0 )
			iwannakillyoubitch=0;
	}

	if(iwannakillyoubitch){
		gsprites.allocated /=2;
		gsprites.sprites = (Sprite **) realloc(gsprites.sprites, sizeof(Sprite *)*gsprites.allocated);//TODO:check return value
	}
}

int GrKill(){
	Config config;
	int i;

	FlushSpriteQueue();
	FlushSpriteSurfaces();

	config=GetConfig();
	free(gsprite_surfaces.surfaces);
	gsprite_surfaces.surfaces=0;
	

	for(i=0; i<gsprites.allocated; i++)
		if(gsprites.sprites[i]){
			free(gsprites.sprites[i]->fname);
			free(gsprites.sprites[i]);
			gsprites.sprites[i]=0;
		}

	gsprites.allocated = 0;


	rsvg_term ();
	SetVideoMode(config.screen_width, config.screen_height);

	return 0;
};

int GrInit(){
	Config config;

	config=GetConfig();
	rsvg_init();
	gsprite_surfaces.surfaces = (GLuint*) calloc(2, sizeof(GLuint));
	if( gsprite_surfaces.surfaces == 0 )
		return 1;
	
	gsprite_surfaces.allocated = 2;
	memset(gsprite_surfaces.surfaces, 0, gsprite_surfaces.allocated * sizeof(GLuint));

	gsprites.sprites = (Sprite**) calloc(2,sizeof(Sprite*));
	if( gsprites.sprites == 0 )
		return 2;
	memset(gsprites.sprites, 0, gsprites.allocated * sizeof(Sprite*));
	gsprites.allocated = 2;
	ginvalidsprites=1;

	gsprite_queue.allocated=2;
	gsprite_queue.items=(SpriteQueueItem*) malloc(sizeof(SpriteQueueItem)*2);
	gsprite_queue.next_id=0;

	SetVideoMode(config.screen_width, config.screen_height);

    glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
    glClearDepth(1.0);
	 glDisable(GL_DEPTH_TEST);
    //glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	 //glEnable(GL_ALPHA_TEST);
	 //glAlphaFunc(GL_GREATER, 0);
	 SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 1);

    glViewport(0, 0, config.screen_width, config.screen_height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0.0f,config.screen_width, config.screen_height,0.0f,-1.0f,1.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

	return 0;
};

void FlushSpriteSurfaces(){
	int i;

	for(i=0;i<gsprite_surfaces.allocated;i++){
		if(gsprite_surfaces.surfaces[i]){
			glDeleteTextures(1, &gsprite_surfaces.surfaces[i]);
			gsprite_surfaces.surfaces[i]=0;
		}
	}
	occupied =0;
}

int RegenerateSprites(){
	int i,SID;
	Config config;
	Sprite *tmpsprite;
	GLuint tmpsurface;
	int xx,yy;

	config=GetConfig();
	

	FlushSpriteSurfaces();

	SetVideoMode(config.screen_width, config.screen_height);

	
	for(i=0;i<gsprites.allocated;i++)
		if(gsprites.sprites[i]){
		tmpsprite=gsprites.sprites[i];
		xx = (tmpsprite->width/SCREEN_WIDTH)*config.screen_width;
		yy = (tmpsprite->height/SCREEN_HEIGHT)*config.screen_height;

		tmpsurface=load_svg(tmpsprite->fname, xx, yy, &tmpsprite->twidth, &tmpsprite->theight);
		
		SID=AddSurface(tmpsurface);
		if(SID==-1){
			return 1;
		}
		RotateSurface(SID,tmpsprite->rotation);
	}
	
	RegenerateFonts();

	ginvalidsprites=0;
	return 0;
};
void InvalidateSprites(){
	printf("Invalid\n");
	ginvalidsprites=1;
};

void FlushSpriteQueue(){
	int i;

	for(i=0;i<gsprite_queue.allocated;i++){
		gsprite_queue.items[i].sprite = 0;
	}
	if(gsprite_queue.next_id < gsprite_queue.allocated/2){
		//TODO:dopsat zmenseni fronty
	}
	gsprite_queue.next_id = 0;
}

void SwapSpriteQueueItems(int aid, int bid){
	SpriteQueueItem c;
	memcpy(&c, &gsprite_queue_sorted.items[aid], sizeof(SpriteQueueItem));
	memcpy(&gsprite_queue_sorted.items[aid], &gsprite_queue_sorted.items[bid], sizeof(SpriteQueueItem));
	memcpy(&gsprite_queue_sorted.items[bid], &c, sizeof(SpriteQueueItem));
}

void YSortSpriteQueue(int from, int to){
	int i;
	int y1, y2;
	int TheEnd = 0;
	//TODO:validity check
	while(!TheEnd){
		TheEnd = 1;
		for(i = from; i < to ; i++){
			y1 = gsprite_queue_sorted.items[i].sprite->height * 0.5 + gsprite_queue_sorted.items[i].y; 
			y2 = gsprite_queue_sorted.items[i+1].sprite->height * 0.5 + gsprite_queue_sorted.items[i+1].y; 
			if(y1 > y2){
				TheEnd = 0;
				SwapSpriteQueueItems(i+1, i);
			}
		}
	}
}

void ZSortSpriteQueue(){
	int minz, lastminz, found;
	int i;
	int ysortfrom, ysortto;
	lastminz = INT_MIN;
	

	found = 1;
	gsprite_queue_sorted.next_id= 0;
	while(found){
		found = 0;
		minz = INT_MAX;
		//we seek for smallest z bigger than z in last iteration
		for(i=0; i<gsprite_queue.next_id; i++){
			if( gsprite_queue.items[i].z < minz && gsprite_queue.items[i].z > lastminz){
				found = 1;
				minz = gsprite_queue.items[i].z;
			}
		}
		if(!found)
			break;
		//now we copy all items on z==minz
		ysortfrom = gsprite_queue_sorted.next_id;
		for(i=0; i < gsprite_queue.next_id; i++){
			if(gsprite_queue.items[i].z == minz){
				memcpy(&gsprite_queue_sorted.items[gsprite_queue_sorted.next_id], &gsprite_queue.items[i], sizeof(SpriteQueueItem));
				ysortto = gsprite_queue_sorted.next_id;
				gsprite_queue_sorted.next_id++;
			}
		}
		YSortSpriteQueue(ysortfrom, ysortto);
		lastminz=minz;
	}
	
}

int DrawSprites(){
	int i;
	SDL_Rect dest;
	SDL_Rect src;
	GLuint ds;
	Sprite *dspr;
	int LastLayer, LayerI;
	
	if(ginvalidsprites){
		printf("invalid\n");
		RegenerateSprites();
	}

	gsprite_queue_sorted.allocated = gsprite_queue.allocated;
	gsprite_queue_sorted.next_id = 0;
	gsprite_queue_sorted.items = malloc(sizeof(SpriteQueueItem)* gsprite_queue_sorted.allocated);
	ZSortSpriteQueue();
	PreDrawTexts();

	// select modulate to mix texture with color for shading
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	glEnable(GL_TEXTURE_2D);

	LastLayer=0;
	for(i=0;i<gsprite_queue_sorted.next_id;i++)
		if (gsprite_queue_sorted.items[i].sprite){
			for (LayerI = LastLayer; LayerI < gsprite_queue_sorted.items[i].z; LayerI++) {
				glDisable(GL_TEXTURE_2D);
				DrawTextsOnLayer(LayerI);
				glEnable(GL_TEXTURE_2D);
			};
			dspr = gsprite_queue_sorted.items[i].sprite;
			ds = gsprite_surfaces.surfaces[dspr->SID];
			dest.x = G2SX(gsprite_queue_sorted.items[i].x);
			dest.y = G2SY(gsprite_queue_sorted.items[i].y);
			dest.w = G2SX(gsprite_queue_sorted.items[i].xx);
			dest.h = G2SX(gsprite_queue_sorted.items[i].yy);
			src.x = 0;
			src.y = 0;
			src.w = G2SX(gsprite_queue_sorted.items[i].sprite->width);
			src.h = G2SX(gsprite_queue_sorted.items[i].sprite->height);


			glBindTexture(GL_TEXTURE_2D, ds);
			glColor4ub(gsprite_queue_sorted.items[i].c.r, gsprite_queue_sorted.items[i].c.g, gsprite_queue_sorted.items[i].c.b, gsprite_queue_sorted.items[i].c.unused);
			TTRECT(dest.x, dest.y, dest.w, dest.h, src.w, src.h, dspr->twidth, dspr->theight);
		}
	
	glDisable(GL_TEXTURE_2D);
	DrawRestOfTexts();

	gsprite_queue_sorted.allocated = 0;
	gsprite_queue_sorted.next_id = 0;
	free(gsprite_queue_sorted.items);
	gsprite_queue_sorted.items = 0;
	FlushSpriteQueue();

	SDL_GL_SwapBuffers();

	return 0;
};

int ClrScr(){
	//SDL_FillRect(gscreen, 0, 0x0);
	return 0;
};

Sprite* LoadSpriteSVG(char *fname, double width, double height){
	Sprite *sprite;
	GLuint surface;
	int xx,yy;
	Config conf=GetConfig();

	xx = width*(double)conf.screen_width/SCREEN_WIDTH;
	yy = height*(double)conf.screen_height/SCREEN_HEIGHT;


	sprite = (Sprite*)malloc(sizeof(Sprite));
	surface = load_svg(fname, xx, yy, &sprite->twidth, &sprite->theight);
	sprite->fname = malloc(sizeof(char)*(strlen(fname)+2));
	strcpy(sprite->fname, fname);
	sprite->width = width;
	sprite->height = height;
	sprite->rotation = 0;
	sprite->SID = AddSurface(surface);
	AddSprite(sprite);

	return sprite;
};
void DestroySprite(Sprite *sprite){
	RemoveSprite(sprite);
	free(sprite->fname);
	sprite->fname=0;
	glDeleteTextures(1, &gsprite_surfaces.surfaces[sprite->SID]);
	RemoveSID(sprite->SID);
	free(sprite);
};

Sprite* CopySprite(Sprite *old){
	printf("CopySprite not Implemented\n");

	return 0;
};


void RotateSurface(int SID, int rotation){
	//int i,x,y;
	//int size;

	fprintf(stderr, "Rotation of sprites not implemented :(\n");
}

void RotateClockwise(Sprite *sprite){
	RotateSurface(sprite->SID,1);
	sprite->rotation=1;
};

void QueueDrawSprite(Sprite *sprite, double x, double y, int z){
	const	SDL_Color white = {255, 255, 255, 255};
	QueueDrawSpriteColorize(sprite, x, y, z, white);
};

void QueueDrawSpriteColorize(Sprite *sprite, double x, double y, int z, SDL_Color color){
	SpriteQueueItem *item;
	if(!sprite){
		printf("NULL sprite*\n");
		return;
	}
	if(gsprite_queue.next_id >=gsprite_queue.allocated){
		item = (SpriteQueueItem *) realloc(gsprite_queue.items, sizeof(SpriteQueueItem)*gsprite_queue.allocated*2);
		if(item == 0){
			fprintf(stderr, "Can't allocate enough memory for sprite queue\n");
			return;
		}
		gsprite_queue.allocated *= 2;
		gsprite_queue.items = item;
	}

	item = &gsprite_queue.items[gsprite_queue.next_id];
	item->sprite = sprite;
	item->x = x;
	item->xx = sprite->width;
	item->y = y;
	item->yy = sprite->height;
	item->z = z;
	item->c = color;

	gsprite_queue.next_id ++;
};

void QueueDrawSpriteColorizeStretch(Sprite *sprite, double x, double y, double xx, double yy, int z, SDL_Color color){
	SpriteQueueItem *item;
	if(!sprite){
		printf("NULL sprite*\n");
		return;
	}
	if(gsprite_queue.next_id >=gsprite_queue.allocated){
		item = (SpriteQueueItem *) realloc(gsprite_queue.items, sizeof(SpriteQueueItem)*gsprite_queue.allocated*2);
		if(item == 0){
			fprintf(stderr, "Can't allocate enough memory for sprite queue\n");
			return;
		}
		gsprite_queue.allocated *= 2;
		gsprite_queue.items = item;
	}

	item = &gsprite_queue.items[gsprite_queue.next_id];
	item->sprite = sprite;
	item->x = x;
	item->xx = xx;
	item->y = y;
	item->yy = yy;
	item->z = z;
	item->c = color;

	gsprite_queue.next_id ++;
};


unsigned int closestpoweroftwo(unsigned int i){
	int p;
	p=0;
	while(i){
		i=i>>1;
		p++;
	}
	return 1<<(p-0);
}


//load_svg borrowed from cairo demo

/* load_svg: This is what you're probably interested in!
 * -----------------------------------------------------
 *  If width and height are greater than 0, the image will
 *  be scaled to that size. wscale and hscale would be ignored.
 *
 *  If width and height are less than 1, wscale and hscale will
 *  resize the width and the height to the specified scales.
 *
 *  If width and height are less than 1, and wscale and hscale
 *  are either 0 or 1, then the image will be loaded at it's
 *  natural size.
 *
 *  See main() for examples.
 */
GLuint load_svg (char *file, int width, int height, double *pdw, double *pdh) {
	RsvgDimensionData g_DimensionData;
	RsvgHandle* rsvghandle;
	GError* pError;

	int bpp;
	int btpp;
	unsigned int rwidth;
	unsigned int rheight;
	unsigned int pw,ph; //power of two dimensions
	double wscale,hscale;
	Config config=GetConfig();

	// Create the SVG cairo stuff.
	rsvghandle = rsvg_handle_new_from_file(file, &pError);

	rsvg_handle_get_dimensions( rsvghandle, &g_DimensionData);
	rwidth = g_DimensionData.width;
	rheight = g_DimensionData.height;
	*pdw = closestpoweroftwo(width);
	*pdh = closestpoweroftwo(height);
	width = width>>config.texture_lod;
	height = height>>config.texture_lod;
	printf("w:%u h:%u\n",width, height);

	/*Calculate final width and height of our surface based on the parameters passed */
	if (width > 0) {
		wscale=(float)width/(float)rwidth;
	} else {
		width=(int)(rwidth*wscale);
	}
	if (height > 0) {
		hscale=(float)height/(float)rheight;
	} else {
		height=(int)(rheight*hscale);
	}


	if(hscale>wscale){
		hscale=wscale;
	}else{
		wscale=hscale;
	}

	/* We will create a CAIRO_FORMAT_ARGB32 surface. We don't need to match
	the screen SDL format, but we are interested in the alpha bit */
	bpp=32; /*bits per pixel*/
	btpp=4; /*bytes per pixel*/

	/* scanline width */
	int stride=width * btpp;

	/* Allocate an image */
	unsigned char *image=calloc(stride*height, 1);

	/* Create the cairo surface with the adjusted width and height */
	cairo_surface_t *cairo_surface;
	cairo_surface = cairo_image_surface_create_for_data (image,
						 CAIRO_FORMAT_ARGB32,
						 width, height, stride);

	cairo_t *cr=cairo_create(cairo_surface);
	cairo_scale (cr, wscale, hscale);

	/* Render SVG to our surface */
	rsvg_handle_render_cairo(rsvghandle, cr);
	rsvg_handle_free(rsvghandle);

	/* Cleanup cairo */
	cairo_surface_destroy (cairo_surface);
	cairo_destroy (cr);

	/*Destroy the svg_cairo structure */

	/*Adjust the SDL surface mask to ARGB, matching the cairo surface created.*/
	Uint32 rmask, gmask, bmask, amask;

	rmask = 0x00ff0000;
	gmask = 0x0000ff00;
	bmask = 0x000000ff;
	amask = 0xff000000;

	/* Create the SDL surface using the pixel data stored. It will automatically be set to use alpha using these mask values */
	SDL_Surface *sdl_surface=SDL_CreateRGBSurfaceFrom( (void *) image, width, height, bpp, stride, rmask, gmask, bmask, amask);
	SDL_SetAlpha(sdl_surface, 0, 255);
	rmask = 0x000000ff;
	gmask = 0x0000ff00;
	bmask = 0x00ff0000;
	amask = 0xff000000;
	pw = closestpoweroftwo(width);
	ph = closestpoweroftwo(height);
	SDL_Surface *sdl_rgba_surface=SDL_CreateRGBSurface( SDL_SWSURFACE, pw, ph, bpp,  rmask, gmask, bmask, amask);
	SDL_BlitSurface(sdl_surface, 0, sdl_rgba_surface, 0);
	SDL_FreeSurface(sdl_surface);
	cfree(image);

	SDL_LockSurface(sdl_rgba_surface);

	GLuint tid;
	glGenTextures(1, &tid);
	glBindTexture(GL_TEXTURE_2D, tid);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); // scale linearly when image bigger than texture
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR); // scale linearly when image smalled than texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, pw, ph, 0, GL_RGBA, GL_UNSIGNED_BYTE, sdl_rgba_surface->pixels);
	SDL_UnlockSurface(sdl_rgba_surface);
	SDL_FreeSurface(sdl_rgba_surface);
	occupied += pw*ph;
	printf("Occupied: %u\n", occupied);

	return tid;
}
