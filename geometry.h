#ifndef GEOMETRY_H_G
#define GEOMETRY_H_G

typedef struct {
	double x,y;
} Vect;

typedef struct {
	Vect *v; //vertexs
	Vect *n;	//normal's for loines between v[i] and v[i+1]
	double *a;	//dot products
	int novec;
	int alloc;
} Poly;

double DotProduct(Vect *a, Vect *b);
void NormV(Vect *v);

int InPoly(Poly *p, Vect *v);	
Poly *NewPoly();
int AddToPoly(Poly *p,Vect *v);
int AddToPolyXY(Poly *p, double x, double y);
void FreePoly(Poly *p);
Poly *NewRect(double x, double y, double xx, double yy);
int PolyInPoly(Poly *p1, Poly *p2); //does they intersect?
int PolyInPolyComp(Poly *p1, Poly *p2); // is one completely inside another? 0 no 1 first in second 2 second in first
Poly *TranslatePolyXY(Poly *p, double x, double y); //returns translated copy of Poly
Poly *TranslatePoly(Poly *p, Vect *v); //returns translated copy of Poly
void MultiplyPoly(Poly *p, double xm, double ym); 
void PrintPoly(Poly *p);
void PrintVect(Vect *a);
char *Poly2String(Poly *p);
void FreePoly2String(char *s); 
Poly *String2Poly(char *s);

typedef struct{
	int nopol;
	Poly **p;
} Area;

Area *NewArea();
void FreeArea(Area *a);
int AddToArea(Area *a, Poly *p);
int InArea(Area *a, Vect *v);	
int AreaInArea(Area *a1, Area *a2); //does they intersect?
int AreaInAreaComp(Area *a1, Area *a2); // is one completely inside another? 0 no 1 first in second 2 second in first
Area *TranslateAreaXY(Area *a, double x, double y); //returns translated copy of Area
Area *TranslateArea(Area *a, Vect *v); //returns translated copy of Area
void MultiplyArea(Area *p, double xm, double ym); 
void PrintArea(Area *a);
char *Area2String(Area *a);
void FreeArea2String(char *s); 
Area *String2Area(char *s);
#endif //GEOMETRY_H_G
