#pragma once

int RegenerateSounds();

void InvalidateSounds();

void PlaySound(int id);

int LoadSound(char *path);

void DestroySound(int id);

void SoundInit();
void SoundKill();

void SoundDestroyAll();
