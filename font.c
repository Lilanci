#include "font.h"
#include "path.h"
#include "gr.h"
#include <FTGL/ftgl.h>
#include <SDL_opengl.h>

typedef struct{
	char *text;
	double x;
	double y;
	int z;			//z
	SDL_Color c;
}TextQueueItem;

typedef struct{
	TextQueueItem *items;
	unsigned int allocated;
	unsigned int next_id;
	unsigned int last_drawn;
}TextQueue;

TextQueue gtext_queue;
TextQueue gtext_queue_sorted;
void FlushTextQueue();
void DrawTextQueueItem(TextQueueItem *item);

FTGLfont *ftfont=0;

void FlushTextQueue(TextQueue *queue) {
	int i;
	for (i = 0; i < queue->allocated; i++) {
		free (queue->items[i].text);
		queue->items[i].text = 0;
	};
	queue->next_id = 0;
	queue->last_drawn = 0;
}

void SwapTextQueueItems(TextQueue *queue, int aid, int bid){
	TextQueueItem c;
	memcpy(&c, &queue->items[aid], sizeof(TextQueueItem));
	memcpy(&queue->items[aid], &queue->items[bid], sizeof(TextQueueItem));
	memcpy(&queue->items[bid], &c, sizeof(TextQueueItem));
}

void GrowTextQueueIfNeeded(TextQueue *queue, int wanted_size){
	TextQueueItem *item;
	while (wanted_size > queue->allocated){
		size_t newsize=0;
		newsize = queue->allocated * 2 + 1;
		item = (TextQueueItem *) realloc(queue->items, sizeof(TextQueueItem)*newsize);
		if(item == 0){
			fprintf(stderr, "Can't allocate enough memory for text queue\n");
			return;
		}
		memset (&item[queue->allocated], 0, sizeof(TextQueueItem) * (newsize - queue->allocated));
		queue->allocated = newsize;
		queue->items = item;
	}
}

void DeleteFonts() {
	if (ftfont!=0) {
		ftglDestroyFont(ftfont);
		ftfont = 0;
	};
}

int RegenerateFonts() {
	DeleteFonts();
	ftfont = ftglCreatePixmapFont(PATH_GRAPHICS "DejaVuSansMono.ttf");
	ftglSetFontFaceSize(ftfont, G2SY(0.5), G2SY(1.0));

	return 0;
}

int DrawTextsOnLayer(int z){
	int i;
	for (i = gtext_queue_sorted.last_drawn + 1; i < gtext_queue_sorted.next_id; i++) {
		if (gtext_queue_sorted.items[i].z > z)
			break;
		DrawTextQueueItem (&gtext_queue_sorted.items[i]);
	}
	gtext_queue_sorted.last_drawn = i - 1;
	return 0;
}

int DrawRestOfTexts(){
	int i;
	for (i = gtext_queue_sorted.last_drawn + 1; i < gtext_queue_sorted.next_id; i++) {
		DrawTextQueueItem (&gtext_queue_sorted.items[i]);
	}
	gtext_queue_sorted.last_drawn = i - 1;
	FlushTextQueue(&gtext_queue);
	FlushTextQueue(&gtext_queue_sorted);
	return 0;
}

int FontInit() {
	ftfont = 0;
	memset (&gtext_queue, 0, sizeof(TextQueue));
	memset (&gtext_queue_sorted, 0, sizeof(TextQueue));
	return 0;
}
int FontKill() {
	DeleteFonts();
	FlushTextQueue(&gtext_queue);
	FlushTextQueue(&gtext_queue_sorted);
	return 0;
}

void QueueDrawTextColorize(char *text, double x, double y, int z, SDL_Color color){
	TextQueueItem *item;
	char *buf;
	if(!text){
		printf("NULL text*\n");
		return;
	}
	GrowTextQueueIfNeeded(&gtext_queue, gtext_queue.next_id+1);
	buf = calloc (strlen (text) + 1, sizeof(char));
	strcpy (buf, text);

	item = &gtext_queue.items[gtext_queue.next_id];
	item->text = buf;
	item->x = x;
	item->y = y;
	item->z = z;
	item->c = color;

	gtext_queue.next_id ++;
}

void QueueDrawText(char *text, double x, double y, int z){
	const	SDL_Color white = {255, 255, 255, 255};
	QueueDrawTextColorize(text, x, y, z, white);
}

void TextQueueCpy (TextQueue *dest, TextQueue *source){
	int i;
	char *buf;
	FlushTextQueue (dest);
	GrowTextQueueIfNeeded (dest, source->next_id);
	memcpy (dest->items, source->items, sizeof (TextQueueItem) * source->next_id);
	dest->last_drawn = -1;
	dest->next_id = source->next_id;
	for (i = 0; i < dest->next_id; i ++)
		if (dest->items[i].text != 0) {
			buf = calloc (strlen (dest->items[i].text) + 1, sizeof(char));
			strcpy (buf, dest->items[i].text);
			dest->items[i].text = buf;
		};
}
static int ZCmpTQI(const void *p1, const void *p2)
{
  return ((const TextQueueItem *)p1)->z - ((const TextQueueItem *)p2)->z;
}

void ZSortTextsQueue(TextQueue *queue){
	qsort ((void *)queue->items, queue->next_id, sizeof(TextQueueItem), ZCmpTQI);
}

void DrawTextQueueItem(TextQueueItem *item){
	glColor4ub(item->c.r, item->c.g, item->c.b, item->c.unused);
	glRasterPos2f(G2SX(item->x), G2SY(item->y));
	ftglRenderFont(ftfont, item->text, FTGL_RENDER_ALL);
}
void DrawText(char *text, double x, double y){
	glRasterPos2f(G2SX(x), G2SY(y));
	ftglRenderFont(ftfont, "H", FTGL_RENDER_ALL);
}

void PreDrawTexts(){
	TextQueueCpy (&gtext_queue_sorted, &gtext_queue);
	ZSortTextsQueue (&gtext_queue_sorted);
}
