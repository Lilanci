#pragma once
#include <SDL/SDL.h>

int RegenerateFonts();
int DrawTextsOnLayer(int i);
int DrawRestOfTexts();
int FontInit();
int FontKill();

void QueueDrawTextColorize(char *text, double x, double y, int z, SDL_Color c);
void QueueDrawText(char *text, double x, double y, int z);
void PreDrawTexts();
