#pragma once
#include "geometry.h"
#include "gr.h"

typedef struct{
	Area **FArea; //forbidden Area
	int noFArea; //no used FArea
}TMapLayer;

typedef struct{
	TMapLayer **Layer;
	int noLayer;
	Area *BoundingArea; // ALL OBJECTS MUST BE IN THIS POLY
	double XX,YY; //margins for random placement
	double X,Y; //left top corner
	int noSprites;
	Sprite **Sprites;
	Vect *SpritePos;
}TMap;

int AreaInMap(TMap *m, Area *p, int FromLayer, int ToLayer); //returns 0 if p collides with something in m otherwise 1
TMap *String2Map(char *String); //returns 0 on failure
char *Map2String(TMap *Map, size_t *bytes); //returns 0 on failure, fills bytes
TMap *LoadMap(char *filename);
void DestroyMap(TMap **Map);

/*
 * Map file structure int,uint--32bit, double--64bit
 * double XX
 * double YY
 * double X
 * double Y
 * uint noSprites
 * uint noLayer
 * uint Layer_seek[noLayer]
 * uint Sprite_seek[noSprite]
 *
 * Layer_seek*:
 * uint noFArea
 * uint string_seek[noFArea]... where to find start of null-terminated string of Forbid Area
 *
 * Sprite_seek*:
 *	double x
 *	double y
 *	uint filename_seek... where to find start of null-terminated string of filename
 */
