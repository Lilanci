#include "particle.h"

double b_x1, b_y1, b_x2, b_y2;

typedef struct plistitem_s{
	Particle *p;
}plistitem_t;

#define NO_PARTS 100
plistitem_t parts[NO_PARTS];

void SetParticleBoundingBox(double x, double y, double xx, double yy)
{
	b_x1 = x;
	b_y1 = y;
	b_x2 = x + xx;
	b_y2 = y + yy;
}

void AddParticle(Particle *p)
{
	int i;

	for (i = 0; i < NO_PARTS; i++) {
		if (parts[i].p == 0) {
			parts[i].p = p;
			return;
		};
	};
	free(p);
}

void UpdateParticle(Particle *p, double dt)
{
	p->x = p->x + dt * p->vx;
	p->vx = p->vx * (1.0 - p->fx*dt) + dt * p->ax;
	p->y = p->y + dt * p->vy;
	p->vy = p->vy * (1.0 - p->fy*dt) + dt * p->ay;
	p->t = p->t + dt * p->vt;
	p->vt = p->vt * (1.0 - p->ft*dt) + dt * p->at;
}

void WhiteFunc(double temp, SDL_Color *c)
{
	static SDL_Color white={255,255,255,255};
	if (c != 0) memcpy(c, &white, sizeof(SDL_Color));
}

void QueueDrawPart(Particle *p)
{
	SDL_Color color;

	if (p->tf != 0){
		p->tf(p->t, &color);
	}else{
		WhiteFunc(p->t, &color);
	};
	QueueDrawSpriteColorize(p->s, p->x, p->y, p->z, color);
}

void RemoveParticle(plistitem_t *a)
{
	if (a == 0) return;
	if (a->p)free(a->p);
	a->p = 0;
}

void UpdateAndQueueDrawParticles(double dt)
{
	plistitem_t *a;
	int i;
	for (i=0; i<NO_PARTS; i++){
		a = &parts[i];
		if (a->p == 0){
			continue;
		};

		UpdateParticle(a->p, dt);

		if (a->p->t <= 0.0 || a->p->x < b_x1 || a->p->y < b_y1 || (a->p->x + a->p->s->width) > b_x2 || (a->p->x + a->p->s->width) > b_x2) {
			RemoveParticle(a);
			continue;
		};

		QueueDrawPart(a->p);
	};
}
