#ifndef UTIL_H_G
#define UTIL_H_G

#define false 0
#define true 1
#include <stdlib.h>

#define RandD(min, max) min+(max-min)*((double)rand()/(double)RAND_MAX)
#define RandI(min, max) min+(max-min)*((double)rand()/(double)RAND_MAX)

#endif //UTIL_H_G
