#pragma once
#include <SDL/SDL.h>
#include <SDL_opengl.h>

#define SCREEN_WIDTH 20.0
#define SCREEN_HEIGHT 15.0


int RegenerateSprites();
//called internally when there is a need for sprites after InvalidateSprites or externally
//returns 0 on success

void InvalidateSprites();
//call it if you want to queue reload of sprites, for example after screen resize

int DrawSprites();
//draw queued spites
//returns 0 on success

int ClrScr();
//Clear Screen -- may does nothing, check code
//returns 0 on success

int GrInit();
//please call this procedure before anything else from this module
//returns 0 on success

int GrKill();
//please call this when you wanna to kill graphics
//also on exit, because not all platforms frees all resources when process ends



typedef struct{
	double width;		//width of a sprite (in game units)
	double height;		//height of a sprite (in game units)
	double twidth;		//height of the texture in pixels
	double theight;	//width of the texture in pixels
	int z;			//z
	char *fname;		//file name of original SVG
	int rotation;		//rotation ( *PI/2.0)
	unsigned int SID; //surface ID
}Sprite;

// Because of lazy implementations of OpenGL on some hw, we are using textures with dimensions of power of two
// if you run out of texture memory, try to lower resolution or config.texture_lod
Sprite* LoadSpriteSVG(char *fname, double width, double height);
//returns 0 when failed

void DestroySprite(Sprite *sprite);
Sprite* CopySprite(Sprite *sprite);

void RotateClockwise(Sprite *sprite); //rotate clockwise by PI/2.0
//currently unimplemented

void QueueDrawSprite(Sprite *sprite, double x, double y, int z);
//queue target sprite to be drawn at [x,y] at level z 0-deepest

void QueueDrawSpriteColorize(Sprite *sprite, double x, double y, int z, SDL_Color c);
//queue target sprite to be drawn at [x,y] at level z 0-deepest
void QueueDrawSpriteColorizeStretch(Sprite *sprite, double x, double y, double xx, double yy, int z, SDL_Color c);
//queue target sprite to be drawn at [x,y] at level z 0-deepest


int G2SX(double x);
int G2SY(double y);
double S2GX(int x);
double S2GY(int y);
