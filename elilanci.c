#include <math.h>
#include <time.h>
#include "geometry.h"
#include "util.h"
#include "config.h"
#include "gr.h"
#include "map.h"
#include "snd.h"
#include "particle.h"
#include "path.h"
#include "font.h"


int gamestate=0;

typedef struct{
	int Up;
	int Down;
	int Left;
	int Right;
	int Shoot;
	int Suicide;
	int Switch;
}TKeyBinding;//TODO: s/int/real data type for a key

typedef struct{
	Vect Pos;	//where he is
	Vect Move; //how he want to move
	Vect BSize; //Size of bounding rect
	Area *Shape;	//how fat he is
	//Area *TrShape;	//translated Shape -- don't optimize yet
	int Frags;	//no comment
	int Deaths;	//R.I.P.
	double Speed;	//meters per second
	int Orientation; //1-up 2-left 3-down 4-right 0-down
	int WannaShoot; //1-want 0-don't want
	double ReloadTime; //time from last shot
	int WannaDie; //1-want suicide 2-died and still holding suicide 0-don't want
	double AnimationTime; //in seconds
	TKeyBinding Keys;
	int *Ammo;
	int ActiveWeapon;
	int SwitchLock;
	int Alive;
	char Name[16];
	SDL_Color Color;
}TLilanek;


typedef struct{
	Vect Pos;
	Area *Shape;
	int WID; //Weapon ID
	int Exists; // 0- doesn't exist 1-exists
	int Ammo;
	int MaxAmmo;
	int Burst;
	int RandomWeight;
	double ReloadTime;
}TWeapon;

typedef struct{
	Vect Pos;
	Area *Shape;
	Vect Vel; //velocity i m/s
	double Speed; //for Templates
	double Range; //How far it can travel
	int WID; //Weapon ID
	int lid; //who shoot this bullet?
	int Exists; // 0- doesn't exist 1-exists
	double SFuzzy; // Fuzzy factor for spawning 0-1 :)
	double VFuzzy; // Fuzzy factor for traveling 0-1 :)
}TBullet;


typedef struct{
	TMap *Map;	//where we are playing
	TLilanek *Lilanek; //our heroes-ehm bad guys
	int noLilaneks;
	
	TWeapon *Weapon; //Weapons lying on floor
	int noWeapons;
	TWeapon *WeaponTemplate;
	TBullet *BulletTemplate;
	int noWID; //biggest WID, also number of WeaponTemplates
	TBullet *Bullet;
	int noBullets;
	double PWeapondrop; // Probability of weapon drop per second
}Game;

Vect Orientations[5];

Sprite *lil[5];
Sprite *bg;
Sprite *kul;
Sprite *explo;
Sprite *dust;
Sprite *blood;
Sprite *brok;
Sprite *sniper;
Sprite *kulka;
Sprite *mine;
Sprite *mines;
Sprite *hud;
Sprite *hud_mine;
Sprite *hud_kul;
Sprite *hud_brok;
Sprite *hud_sniper;
Sprite *hud_ammo;
Sprite *hud_reload;
int s_die;
int s_bonus;
int s_expl;
int s_shoot;
int s_mine;

int frames=0;
void SwitchNextWeapon(Game *g, int lid);

int InitBase(){
	Orientations[0].x=0;
	Orientations[0].y=0;
	Orientations[1].x=0;
	Orientations[1].y=-1;
	Orientations[2].x=-1;
	Orientations[2].y=0;
	Orientations[3].x=0;
	Orientations[3].y=1;
	Orientations[4].x=1;
	Orientations[4].y=0;
	//TODO: write code
	
	return 0;
}

void SpawnLilanek(Game *g, int lid){
	Vect Pos;
	int tries;
	Area *tp, *oltp;
	int good;
	int olid;

	oltp = 0;
	tp = 0;
	for(tries=0; tries <1000; tries++){ //TODO: remove exact number
		Pos.x = RandD(0,g->Map->XX);
		Pos.y = RandD(0,g->Map->YY);
		good=1;
		tp = TranslateArea(g->Lilanek[lid].Shape, &Pos);
		printf("%s\n", Area2String(tp));
		if(!AreaInMap(g->Map, tp,0,1)){
			good=0;
		}
		
		for(olid=0; olid < g->noLilaneks; olid++){
			if(olid == lid)
				continue;
			if(!g->Lilanek[olid].Alive){
				continue;
			}
			oltp = TranslateArea(g->Lilanek[olid].Shape, &g->Lilanek[olid].Pos);
			if(AreaInArea(tp, oltp)){
				good=0;
			}
			FreeArea(oltp);
			oltp = 0;
		}

		if(good){
			FreeArea(tp);
			tp=0;
			g->Lilanek[lid].Pos.x = Pos.x;
			g->Lilanek[lid].Pos.y = Pos.y;
			g->Lilanek[lid].Alive = 1;
			break;
		}
		FreeArea(tp);
		tp=0;
	}
}

int RandWID(Game *g) {
	int SumRandWeight = 0;
	int i, j;

	for (i=0; i < g->noWID; i++) {
		SumRandWeight += g->WeaponTemplate[i].RandomWeight;
	}
	j = RandI(0, SumRandWeight);
	for (i=0; i < g->noWID; i++) {
		j -= g->WeaponTemplate[i].RandomWeight;
		if (j <= 0)
			return i;
	}
	return 0;
}

void DropWeapon(Game *g, int weapon){
	Vect Pos;
	int tries;
	int WID;
	Area *tp;

	for(tries=0; tries <100; tries++){ //TODO: remove exact number
		Pos.x = RandD(0,g->Map->XX);
		Pos.y = RandD(0,g->Map->YY);
		WID = RandWID(g);
		tp = TranslateArea(g->WeaponTemplate[WID].Shape, &Pos);
		if(AreaInMap(g->Map, tp, 0, g->Map->noLayer)){
			FreeArea(tp);
			tp=0;
			memcpy(&g->Weapon[weapon], &g->WeaponTemplate[WID], sizeof(TWeapon));
			g->Weapon[weapon].Shape = g->WeaponTemplate[WID].Shape;
			g->Weapon[weapon].Pos.x = Pos.x;
			g->Weapon[weapon].Pos.y = Pos.y;
			g->Weapon[weapon].WID = WID;
			g->Weapon[weapon].Exists = 1;
			break;
		}
		FreeArea(tp);
		tp=0;
	}
}

void DropWeapons(Game *g, double dtime){
	int i;
	
	if( RandD(0.0, 1.0) >= (g->PWeapondrop*dtime))
		return;
	for(i=0; i < g->noWeapons; i++){
		if(g->Weapon[i].Exists)continue; //we don't like teleporting weapons :)
		
		printf("A Weapon drop!\n");
		DropWeapon(g, i);
		break; //spawn max one weapon per update
	}
}

void WeaponPickedUp(Game *g, double dtime, int lid, int wid){
	TLilanek *l;
	TWeapon *w;

	l = &g->Lilanek[lid];
	w = &g->Weapon[wid];

	l->Ammo[w->WID] += w->Ammo;
	if(l->Ammo[w->WID] > w->MaxAmmo)
		l->Ammo[w->WID] = w->MaxAmmo;
	printf("Ammo: %d\n",l->Ammo[w->WID]);
	l->ActiveWeapon = w->WID;
	printf("AW: %d\n",l->ActiveWeapon);

	w->Exists = 0;

	PlaySound(s_bonus);
}

void PickUpWeapons(Game *g, double dtime){
	int lid,wid;
	Area *WArea, *LArea;

	for(lid=0; lid<g->noLilaneks; lid++){
		LArea = TranslateArea(g->Lilanek[lid].Shape, &g->Lilanek[lid].Pos);
		for(wid=0; wid < g->noWeapons; wid++){
			if(!g->Weapon[wid].Exists)
				continue;
			WArea = TranslateArea(g->Weapon[wid].Shape, &g->Weapon[wid].Pos);
			if(!AreaInArea( WArea, LArea)){
				FreeArea(WArea);
				continue;
			}
			WeaponPickedUp(g, dtime, lid, wid);
			FreeArea(WArea);
		}
		FreeArea(LArea);
	}
}

void BulletExplodes(Game *g, int i){
	g->Bullet[i].Exists=0;
	//TODO: some effects
	PlaySound (s_expl);
}

void MoveBullet(Game *g, double dtime, int i){
	double ff;
	Vect dp;
	dp.x = g->Bullet[i].Pos.x;
	dp.y = g->Bullet[i].Pos.y;
	ff= g->Bullet[i].Speed*g->Bullet[i].VFuzzy;
	g->Bullet[i].Pos.x += (g->Bullet[i].Vel.x += RandD(-ff,ff))* dtime; 
	g->Bullet[i].Pos.y += (g->Bullet[i].Vel.y += RandD(-ff,ff))* dtime; 
	dp.x -= g->Bullet[i].Pos.x;
	dp.y -= g->Bullet[i].Pos.y;
	g->Bullet[i].Range -= sqrt(DotProduct(&dp, &dp));
}

void CollideBulletMap(Game *g, double dtime, int i){
	Area *p;
	
	p=TranslateArea(g->Bullet[i].Shape, &g->Bullet[i].Pos);
	if(AreaInMap(g->Map, p, 1,1)==0)
		BulletExplodes(g, i);
	FreeArea(p);
}

void BloodColorFunc(double temp, SDL_Color *c)
{
	SDL_Color col={255,255,255,255};
	col.r *=(0.5+0.5*temp);
	//col.g *=temp;
	//col.b *=temp;
	//col.unused *=temp*temp;
	if (c != 0) memcpy(c, &col, sizeof(SDL_Color));
}

void BloodParticles(Game *g, double dtime, int lid)
{
	int i;
	double a;
	double v;
	Particle *p;

	for(i = 0; i < 5; i ++){
		p = malloc(sizeof(Particle));
		memset(p, 0, sizeof(Particle));
		a = RandD(0.0, 20.0);
		v = RandD(0.0, 2.0);
		p->x = g->Lilanek[lid].Pos.x-g->Lilanek[lid].BSize.x*0.5;
		p->y = g->Lilanek[lid].Pos.y-g->Lilanek[lid].BSize.y*0.5;
		p->t = 1.0;
		p->vx = v*sin(a) + g->Lilanek[lid].Move.x;
		p->vy = v*cos(a) + g->Lilanek[lid].Move.y;
		p->fx = 1.5;
		p->fy = 1.5;
		p->vt = 0.0-1.0;
		p->s = blood;
		p->tf = &BloodColorFunc;
		p->z = 1;
		AddParticle(p);
	};
}

void LilanekKilled(Game *g, double dtime, int BulletID, int LilanekID){
	int i;
	g->Bullet[BulletID].Exists = 0;
	g->Lilanek[LilanekID].Deaths ++;
	if (LilanekID != g->Bullet[BulletID].lid) {
		g->Lilanek[g->Bullet[BulletID].lid].Frags ++;
	}else{ //self-kill
		g->Lilanek[g->Bullet[BulletID].lid].Frags --;
	}
	BloodParticles(g, dtime, LilanekID);
	SpawnLilanek(g, LilanekID);
	for(i=0; i<g->noWID; i++){
		g->Lilanek[LilanekID].Ammo[i] = 0;
	};
	PlaySound(s_die);
}

#define NO_EXPLOSIONS 10 //MAGIC_NUMBER
#define EXPLOSION_SPEED 5 //MAGIC_NUMBER
#define EXPLOSION_RANGE 1.0 //MAGIC_NUMBER

void SpawnExplosions(Game *g, double dtime, int BulletID){
	int bid = 0;
	int i = 0;
	TBullet *b;
	Vect Pos;
	double speed;

	Pos.x = g->Bullet[BulletID].Pos.x;
	Pos.y = g->Bullet[BulletID].Pos.y;
	for(i=0; i<NO_EXPLOSIONS; i++){
		for(bid=0; bid<g->noBullets; bid++){
			if(g->Bullet[bid].Exists)
				continue;
			b = &g->Bullet[bid];
			b->Pos.x = Pos.x;
			b->Pos.y = Pos.y;
			b->Vel.x = RandD(0.0, 1.0)*(RandI(0,100)>50?-1:1);
			b->Vel.y = RandD(0.0, 1.0)*(RandI(0,100)>50?-1:1);
			NormV(&b->Vel);
			speed = RandD(0.3, 1.0)*EXPLOSION_SPEED;
			b->Speed = speed;
			b->Vel.x *= speed;
			b->Vel.y *= speed;
			b->WID = -1;
			b->lid = g->Bullet[BulletID].lid;
			b->Exists = 1;
			b->SFuzzy = 0;
			b->VFuzzy = 0;
			b->Range = EXPLOSION_RANGE;
			b->Shape = g->Bullet[BulletID].Shape;
			printf("%lf ", speed);
			break;
		}
	};
	g->Bullet[BulletID].Exists = 0;
	PlaySound(s_expl);
}

void CollideBulletLilanek(Game *g, double dtime, int BulletID){
	Area *BArea, *LArea;
	int LilanekID;
	
	BArea=TranslateArea(g->Bullet[BulletID].Shape, &g->Bullet[BulletID].Pos);
	for(LilanekID=0; LilanekID<g->noLilaneks; LilanekID++){
		LArea=TranslateArea(g->Lilanek[LilanekID].Shape, &g->Lilanek[LilanekID].Pos);
		if(AreaInArea(LArea,BArea)){
			//if it is a mine, spawn an explosion instead of killing instantly
			if(g->Bullet[BulletID].WID == 1){
				SpawnExplosions(g, dtime, BulletID);
			}else{
				LilanekKilled(g, dtime, BulletID, LilanekID);
			};
		};
		FreeArea(LArea);
		LArea=0;
	}
	FreeArea(BArea);
	BArea=0;
}

void UpdateBullets(Game *g, double dtime){
	int BulletID;
	for(BulletID=0; BulletID<g->noBullets; BulletID++){
		if(g->Bullet[BulletID].Exists==0)
			continue; //We won't update non-existending bullets
		MoveBullet(g, dtime, BulletID);
		if(g->Bullet[BulletID].Range < 0){
			g->Bullet[BulletID].Exists = 0;
			continue;
		};
		CollideBulletMap(g, dtime, BulletID);
		CollideBulletLilanek(g, dtime, BulletID);
	}
}

int CheckLilanekvsLilaneks(Game *g, Area *a, int LilanekID){ //returns 0 if he collides with others, 1 if it's OK to move there
	int i;
	Area *nb;
	int rval=1;
	
	nb=0;
	for(i=0; i<g->noLilaneks && rval==1; i++){
		if(i==LilanekID)
			continue;
		nb = TranslateArea(g->Lilanek[i].Shape, &g->Lilanek[i].Pos);
		if(AreaInArea(nb, a))
			rval=0;
		FreeArea(nb);
		nb=0;
	}
	return rval;
}

void MoveLilaneks(Game *g, double dtime){
	int i, steps;
	Area *pa;
	Vect va;
	double dt;

	pa=0;
	for(i=0; i<g->noLilaneks; i++){
		//skip this Lilanek if he doesn't wanna move
		if(g->Lilanek[i].Move.x == 0.0 && g->Lilanek[i].Move.y == 0.0)
			continue;
		for(dt=dtime, steps=0; steps<4; dt/=2.0, steps++){ //TODO: get rid of exact number
			//make future Area
			va.x = g->Lilanek[i].Pos.x + dt*g->Lilanek[i].Move.x;
			va.y = g->Lilanek[i].Pos.y + dt*g->Lilanek[i].Move.y;
			FreeArea(pa); //we don't want memory leaks
			pa = TranslateArea(g->Lilanek[i].Shape, &va);
			//check for collision with map
			if(AreaInMap(g->Map, pa, 0, 1)==0)
				continue; //try smaller dt if he collided with map
			//check for collision with other Lilaneks
			if(CheckLilanekvsLilaneks(g, pa, i)==0) 
				continue;
			//move him if we got here
			g->Lilanek[i].Pos.x += dt*g->Lilanek[i].Move.x;
			g->Lilanek[i].Pos.y += dt*g->Lilanek[i].Move.y;
			FreeArea(pa);
			pa=0;
		}
		FreeArea(pa); //For sure
		pa=0;
	}
}

void ParseInput(Game *g, double dtime){
	int i;
	Uint8 *keystate;
	TLilanek *curlil;
	int wannamove;

	keystate = SDL_GetKeyState(NULL);
	//pause
	if(gamestate == 1){
		if(keystate['o']){
			printf("unpause\n");
			gamestate = 0;
		}else return;
	}
	if(keystate['p']){
		printf("pause\n");
		gamestate = 1;
	}

	for(i=0; i< g->noLilaneks; i++){
		curlil = &g->Lilanek[i];
		wannamove=0;
		// Up;
		if(keystate[curlil->Keys.Up]){
			curlil->Orientation = 1;
			curlil->Move.x = curlil->Speed * Orientations[1].x;
			curlil->Move.y = curlil->Speed * Orientations[1].y;
			wannamove=1;
		}
		// Down;
		if(keystate[curlil->Keys.Down]){
			curlil->Orientation = 3;
			curlil->Move.x = curlil->Speed * Orientations[3].x;
			curlil->Move.y = curlil->Speed * Orientations[3].y;
			wannamove=1;
		}
		// Left;
		if(keystate[curlil->Keys.Left]){
			curlil->Orientation = 2;
			curlil->Move.x = curlil->Speed * Orientations[2].x;
			curlil->Move.y = curlil->Speed * Orientations[2].y;
			wannamove=1;
		}
		// Right;
		if(keystate[curlil->Keys.Right]){
			curlil->Orientation = 4;
			curlil->Move.x = curlil->Speed * Orientations[4].x;
			curlil->Move.y = curlil->Speed * Orientations[4].y;
			wannamove=1;
		}

		if(!wannamove){
			curlil->Move.x = 0;
			curlil->Move.y = 0;
		}
		// Shoot;
		if(keystate[curlil->Keys.Shoot]){
			curlil->WannaShoot = 1;
		}else{
			curlil->WannaShoot = 0;
		}
		// Suicide;
		if(keystate[curlil->Keys.Suicide]){
			if(curlil->WannaDie != 2){
				curlil->WannaDie = 1;
			}
		}else{
			curlil->WannaDie = 0;
		}
		// Switch;
		if(keystate[curlil->Keys.Switch]){
			if(!curlil->SwitchLock){
				SwitchNextWeapon(g, i);
				curlil->SwitchLock = 1;
			}
		}else{
			curlil->SwitchLock = 0;
		}

	}
}
void SpawnBullet(Game *g, double dtime, int lid, int bid){
	Area *LArea, *BArea;
	TBullet *b, *bt;
	double vx, vy; //because we need to spawn also mines with no velocity

	
	LArea = TranslateArea(g->Lilanek[lid].Shape, &g->Lilanek[lid]. Pos);
	b = &g->Bullet[bid];
	bt = &g->BulletTemplate[g->Lilanek[lid].ActiveWeapon];
	memcpy (b, bt, sizeof(TBullet));
	b->Exists = 1;
	b->Pos.x = g->Lilanek[lid].Pos.x+g->Lilanek[lid].BSize.x*0.5; 
	b->Pos.y = g->Lilanek[lid].Pos.y+g->Lilanek[lid].BSize.y*0.5;
	b->WID = bt->WID;
	b->Range = bt->Range;
	b->Vel.x = Orientations[g->Lilanek[lid].Orientation].x * b->Speed +RandD(-b->Speed*b->SFuzzy, b->Speed*b->SFuzzy);
	b->Vel.y = Orientations[g->Lilanek[lid].Orientation].y * b->Speed +RandD(-b->Speed*b->SFuzzy, b->Speed*b->SFuzzy);
	if(DotProduct(&b->Vel, &b->Vel) <=0.01){
		vx = -1*Orientations[g->Lilanek[lid].Orientation].x;
		vy = -1*Orientations[g->Lilanek[lid].Orientation].y;
	}else{
		vx = b->Vel.x;
		vy = b->Vel.y;
	};
	b->lid =lid;
	while(1){
		BArea = TranslateArea(b->Shape, &b->Pos);
		if(!AreaInArea(BArea, LArea))
			break;
		b->Pos.x += vx* dtime*0.5;
		b->Pos.y += vy* dtime*0.5;
	}
	FreeArea(LArea);
};

void DustColorFunc(double temp, SDL_Color *c)
{
	SDL_Color col={255,255,255,255};
	col.r *=temp;
	col.g *=temp;
	col.b *=temp;
	col.unused *=temp*temp;
	if (c != 0) memcpy(c, &col, sizeof(SDL_Color));
}

void ShootParticles(Game *g, double dtime, int lid)
{
	int i;
	double a;
	double v;
	Particle *p;

	for(i = 1; i < 10; i ++){
		p = malloc(sizeof(Particle));
		memset(p, 0, sizeof(Particle));
		a = RandD(0.0, 2*M_PI);
		v = RandD(0.0, 2.0);
		p->x = g->Lilanek[lid].Pos.x-g->Lilanek[lid].BSize.x*0.5;
		p->y = g->Lilanek[lid].Pos.y-g->Lilanek[lid].BSize.y*0.5;
		p->t = 1.0;
		p->vx = v*sin(a) + g->Lilanek[lid].Move.x;
		p->vy = v*cos(a) + g->Lilanek[lid].Move.y;
		p->vt = 0.0-2.0;
		p->s = dust;
		p->z = 1;
		p->tf = &DustColorFunc;
		AddParticle(p);
	};
}

void Shoot(Game *g, double dtime, int lid){
	int bid;
	int i;

	for(i=0; i<g->WeaponTemplate[g->Lilanek[lid].ActiveWeapon].Burst; i++){
		if(g->Lilanek[lid].Ammo[g->Lilanek[lid].ActiveWeapon] == 0)
			return;
		for(bid=0; bid<g->noBullets; bid++){
			if(g->Bullet[bid].Exists)
				continue;
			SpawnBullet(g, dtime, lid, bid);
			g->Lilanek[lid].Ammo[g->Lilanek[lid].ActiveWeapon]--;
			g->Lilanek[lid].ReloadTime = 0;
			break;
		}
	}
	ShootParticles(g, dtime, lid);
	switch(g->Lilanek[lid].ActiveWeapon){
		case 0: PlaySound(s_shoot); break;
		case 1: PlaySound(s_mine); break;
		case 2: PlaySound(s_shoot); break;
		default: break;
	}
}

void SwitchNextWeapon(Game *g, int lid){
	int i;
	TLilanek *l;
	l = &g->Lilanek[lid];
	for(i = 1; i < g->noWID; i++){
		if(l->Ammo[(l->ActiveWeapon+i)%(g->noWID)]){
			l->ActiveWeapon = (l->ActiveWeapon+i)%(g->noWID);
			return;
		}
	}
}

void ShootIfTheyWantTo(Game *g, double dtime){
	int lid;

	for(lid=0; lid<g->noLilaneks; lid++){
		//if(!g->Lilanek[lid].Exists)
			//continue;
		if(g->Lilanek[lid].Ammo[g->Lilanek[lid].ActiveWeapon] == 0)
			SwitchNextWeapon(g,lid);

		if(g->Lilanek[lid].ReloadTime < g->WeaponTemplate[g->Lilanek[lid].ActiveWeapon].ReloadTime){
			g->Lilanek[lid].ReloadTime += dtime;
		}else{
			if(!g->Lilanek[lid].WannaShoot)
				continue;
			Shoot(g, dtime, lid);
		}
	}
}

int UpdateLogic(Game *g,double dtime){
	if(gamestate == 0){
		DropWeapons(g, dtime);
		UpdateBullets(g, dtime);
	};
	ParseInput(g, dtime); 
	if(gamestate == 0){
		MoveLilaneks(g, dtime);
		PickUpWeapons(g, dtime);
		ShootIfTheyWantTo(g, dtime);
	};
	return 0; //TODO: return something useful
}

Game *testgame(){ //return testing game, function for use before we have some menu/loading mechanism
	Game *g;
	Poly *p, *pp;
	Area *a;
	int i;
	Config conf = GetConfig();
	
	g = malloc(sizeof(Game));
	memset(g, 0, sizeof(Game));
	g->noWID=4;
	g->Map = malloc(sizeof(TMap));
	g->Map->noLayer=2;
	g->Map->Layer = malloc(sizeof(TMapLayer *)*g->Map->noLayer);
	g->Map->Layer[0] = malloc(sizeof(TMapLayer));
	g->Map->Layer[0]->noFArea = 1;
	g->Map->Layer[0]->FArea = malloc(sizeof(Area *) * g->Map->Layer[0]->noFArea);
	p = NewRect(4,2.5,3,2);
	a = NewArea();
	AddToArea(a, p);
	FreePoly(p);
	g->Map->Layer[0]->FArea[0] = a;

	g->Map->Layer[1] = malloc(sizeof(TMapLayer));
	g->Map->Layer[1]->noFArea = 1;
	g->Map->Layer[1]->FArea = malloc(sizeof(Area *) * g->Map->Layer[1]->noFArea);
	p = NewPoly();
	AddToPolyXY(p, 0.30, 0.41);
	AddToPolyXY(p, 0.73, 0.41);
	AddToPolyXY(p, 0.77, 0.79);
	AddToPolyXY(p, 0.65, 0.955);
	AddToPolyXY(p, 0.365, 0.965);
	AddToPolyXY(p, 0.235, 0.785);
	MultiplyPoly(p, 4, 4);
	pp = TranslatePolyXY(p, 7, 7);
	a = NewArea();
	AddToArea(a, pp);
	FreePoly(pp);
	FreePoly(p);
	g->Map->Layer[1]->FArea[0] = a;
	p = NewRect(1,1,16,15);
	a = NewArea();
	AddToArea(a, p);
	FreePoly(p);
	g->Map->BoundingArea = a;
	g->Map->XX = 16;
	g->Map->YY = 15;
	g->Map->X = 0;
	g->Map->Y = 0;

	SetParticleBoundingBox(0,0,16,15);



	g->Map->noSprites = 2;
	g->Map->Sprites = (Sprite **) malloc(sizeof(Sprite*)*g->Map->noSprites);
	g->Map->SpritePos = (Vect *) malloc(sizeof(Vect)*g->Map->noSprites);
	g->Map->SpritePos[0].x = 3.0;
	g->Map->SpritePos[0].y = 1.0;
	g->Map->SpritePos[1].x = 6.0;
	g->Map->SpritePos[1].y = 6.0;

	g->noWeapons = 8;
	g->Weapon = malloc(sizeof(TWeapon) * g->noWeapons);
	if(!g->Weapon){
		fprintf(stderr, "Cannot allocate memory for Weapons\n");
	}
	for(i=0; i<g->noWeapons; i++){
		g->Weapon[i].Exists = 0;
		g->Weapon[i].Shape = 0;
	}
	
	g->noLilaneks = 2;
	g->Lilanek = malloc(sizeof(TLilanek) * g->noLilaneks);
	memset(g->Lilanek, 0, sizeof(TLilanek) * g->noLilaneks);
	g->Lilanek[0].BSize.x = 1;
	g->Lilanek[0].BSize.y = 1;
	//g->Lilanek[0].Shape = NewRect(0.6,0.2,0.8,1.6);
	p = NewPoly();
	AddToPolyXY(p, 1, 0);
	AddToPolyXY(p, 1.4, 0.2);
	AddToPolyXY(p, 1.66, 1.38);
	AddToPolyXY(p, 1.42, 2);
	AddToPolyXY(p, 0.6, 2);
	AddToPolyXY(p, 0.4, 1.46);
	AddToPolyXY(p, 0.66, 0.2);
	MultiplyPoly(p, 0.5*g->Lilanek[0].BSize.x, 0.5*g->Lilanek[0].BSize.y);
	a = NewArea();
	AddToArea(a, p);
	FreePoly(p);
	g->Lilanek[0].Shape = a;
	g->Lilanek[0].Pos.x = 12;
	g->Lilanek[0].Pos.y = 4;
	g->Lilanek[0].Speed = 4;
	g->Lilanek[0].Alive = 0;
	g->Lilanek[0].Keys.Up = conf.pl1_key_up;
	g->Lilanek[0].Keys.Down = conf.pl1_key_down;
	g->Lilanek[0].Keys.Left = conf.pl1_key_left;
	g->Lilanek[0].Keys.Right = conf.pl1_key_right;
	g->Lilanek[0].Keys.Shoot = conf.pl1_key_fire;
	g->Lilanek[0].Keys.Suicide = conf.pl1_key_suicide;
	g->Lilanek[0].Keys.Switch = conf.pl1_key_switch;
	g->Lilanek[0].Ammo = malloc(sizeof(int)*g->noWID);
	for(i=0; i<g->noWID; i++){
		g->Lilanek[0].Ammo[i] = 0;
	}
	g->Lilanek[0].ActiveWeapon = 0;

	SDL_Color red = {255,211,211,255};
	char redn[8]="Red";
	g->Lilanek[0].Color = red;
	strcpy(g->Lilanek[0].Name, redn);

	g->Lilanek[1].Alive = 0; // little hack for SpawnLilanek to work
	SpawnLilanek(g, 0);

	p = g->Lilanek[0].Shape->p[0];
	a = NewArea();
	AddToArea(a, p);
	g->Lilanek[1].Shape = a;
	g->Lilanek[1].BSize.x = g->Lilanek[0].BSize.x;
	g->Lilanek[1].BSize.y = g->Lilanek[0].BSize.y;
	g->Lilanek[1].Pos.x = 14;
	g->Lilanek[1].Pos.y = 4;
	g->Lilanek[1].Speed = 4;
	g->Lilanek[1].Alive = 0;
	g->Lilanek[1].Keys.Up = conf.pl2_key_up;
	g->Lilanek[1].Keys.Down = conf.pl2_key_down;
	g->Lilanek[1].Keys.Left = conf.pl2_key_left;
	g->Lilanek[1].Keys.Right = conf.pl2_key_right;
	g->Lilanek[1].Keys.Shoot = conf.pl2_key_fire;
	g->Lilanek[1].Keys.Suicide = conf.pl2_key_suicide;
	g->Lilanek[1].Keys.Switch = conf.pl2_key_switch;
	g->Lilanek[1].Ammo = malloc(sizeof(int)*g->noWID);
	for(i=0; i<g->noWID; i++){
		g->Lilanek[1].Ammo[i] = 0;
	}
	g->Lilanek[1].ActiveWeapon = 0;

	SDL_Color blue = {180,180,255, 255};
	char bluen[8]="Blue";
	g->Lilanek[1].Color = blue;
	strcpy(g->Lilanek[1].Name, bluen);

	SpawnLilanek(g, 1);

	g->WeaponTemplate = malloc(sizeof(TWeapon)*g->noWID);
	p = NewRect(0,0,0.5,0.5);
	a = NewArea();
	AddToArea(a, p);
	FreePoly(p);
	g->WeaponTemplate[0].Shape = a;
	g->WeaponTemplate[0].WID = 0;
	g->WeaponTemplate[0].Exists = 1;
	g->WeaponTemplate[0].Ammo = 30;
	g->WeaponTemplate[0].MaxAmmo = 120;
	g->WeaponTemplate[0].ReloadTime = 0.7;
	g->WeaponTemplate[0].Burst = 10;
	g->WeaponTemplate[0].RandomWeight = 10;

	g->BulletTemplate = malloc(sizeof(TBullet) * g->noWID);

	// shotgun
	p = NewRect(0,0,0.1,0.1);
	a = NewArea();
	AddToArea(a, p);
	FreePoly(p);
	g->BulletTemplate[0].Shape = a;
	g->BulletTemplate[0].WID = 0;
	g->BulletTemplate[0].Speed = 20;
	g->BulletTemplate[0].Range = 20;
	g->BulletTemplate[0].SFuzzy = 0.160;
	g->BulletTemplate[0].VFuzzy = 0.000;

	// mine
	p = NewRect(0,0,0.5,0.5);
	a = NewArea();
	AddToArea(a, p);
	FreePoly(p);
	g->WeaponTemplate[1].Shape = a;
	g->WeaponTemplate[1].WID = 1;
	g->WeaponTemplate[1].Exists = 1;
	g->WeaponTemplate[1].Ammo = 3;
	g->WeaponTemplate[1].MaxAmmo = 6;
	g->WeaponTemplate[1].ReloadTime = 0.5;
	g->WeaponTemplate[1].Burst = 1;
	g->WeaponTemplate[1].RandomWeight = 5;

	p = NewRect(0,0,0.5,0.5);
	a = NewArea();
	AddToArea(a, p);
	FreePoly(p);
	g->BulletTemplate[1].Shape = a;
	g->BulletTemplate[1].WID = 1;
	g->BulletTemplate[1].Speed = 0;
	g->BulletTemplate[1].Range = 20;
	g->BulletTemplate[1].SFuzzy = 0.0;
	g->BulletTemplate[1].VFuzzy = 0.0;

	// uzi
	p = NewRect(0,0,0.5,0.5);
	a = NewArea();
	AddToArea(a, p);
	FreePoly(p);
	g->WeaponTemplate[2].Shape = a;
	g->WeaponTemplate[2].WID = 0;
	g->WeaponTemplate[2].Exists = 1;
	g->WeaponTemplate[2].Ammo = 10;
	g->WeaponTemplate[2].MaxAmmo = 50;
	g->WeaponTemplate[2].ReloadTime = 0.1;
	g->WeaponTemplate[2].Burst = 1;
	g->WeaponTemplate[2].RandomWeight = 15;

	p = NewRect(0,0,0.1,0.1);
	a = NewArea();
	AddToArea(a, p);
	FreePoly(p);
	g->BulletTemplate[2].Shape = a;
	g->BulletTemplate[2].WID = 0;
	g->BulletTemplate[2].Speed = 20;
	g->BulletTemplate[2].Range = 20;
	g->BulletTemplate[2].SFuzzy = 0.040;
	g->BulletTemplate[2].VFuzzy = 0.010;

	// sniper rifle
	p = NewRect(0,0,0.5,0.5);
	a = NewArea();
	AddToArea(a, p);
	FreePoly(p);
	g->WeaponTemplate[3].Shape = a;
	g->WeaponTemplate[3].WID = 0;
	g->WeaponTemplate[3].Exists = 1;
	g->WeaponTemplate[3].Ammo = 5;
	g->WeaponTemplate[3].MaxAmmo = 20;
	g->WeaponTemplate[3].ReloadTime = 1.1;
	g->WeaponTemplate[3].Burst = 1;
	g->WeaponTemplate[3].RandomWeight = 10;

	p = NewRect(0,0,0.1,0.1);
	a = NewArea();
	AddToArea(a, p);
	FreePoly(p);
	g->BulletTemplate[3].Shape = a;
	g->BulletTemplate[3].WID = 0;
	g->BulletTemplate[3].Speed = 50;
	g->BulletTemplate[3].Range = 200;
	g->BulletTemplate[3].SFuzzy = 0.000;
	g->BulletTemplate[3].VFuzzy = 0.000;

	g->noBullets = 64;
	g->Bullet = malloc(sizeof(TBullet) * g->noBullets);
	for(i=0; i<g->noBullets; i++){
		g->Bullet[i].Exists = 0;
		g->Bullet[i].Shape = 0;
	}

	g->PWeapondrop = 0.4;

	return g;
}


void onExit()
{
	GrKill();
	SDL_Quit();
}

Uint32 callback_fps(Uint32 interval, void *param)
{
        SDL_Event event;
        event.type = SDL_USEREVENT;
        SDL_PushEvent(&event);

        return 1000;
}

SDL_Color InvertColor(SDL_Color c) {
	SDL_Color t;
	t.r = 255 - c.r;
	t.g = 255 - c.g;
	t.b = 255 - c.b;
	t.unused = c.unused;
	return t;
}

void Draw(Game *g){
	int i;
	
	QueueDrawSprite(bg, 0.0, 0.0,-1.0);
	for(i=0; i < g->Map->noSprites; i++)
		QueueDrawSprite(g->Map->Sprites[i], g->Map->SpritePos[i].x, g->Map->SpritePos[i].y, g->Map->Sprites[i]->z);
	//Lilaneks
	for(i=0; i < g->noLilaneks; i++){
		SDL_Color c;
		double pd;
		char fragsbuf[16];
		c =g->Lilanek[i].Color;
		if(g->Lilanek[i].ReloadTime < g->WeaponTemplate[g->Lilanek[i].ActiveWeapon].ReloadTime){
			c.r *= 0.5;
			c.g *= 0.5;
			c.b *= 0.5;
			c.unused *= 0.8;
		};
		QueueDrawSpriteColorize(lil[g->Lilanek[i].Orientation], g->Lilanek[i].Pos.x-1.0, g->Lilanek[i].Pos.y-1.0, 1, c );
		QueueDrawSpriteColorize(hud,16.0,4.0*i, 1.0, c );
		switch(g->Lilanek[i].ActiveWeapon){
			case 0:	QueueDrawSprite(hud_brok, 16.0, 0.0+4.0*i,0.0);
						break;
			case 1:	QueueDrawSprite(hud_mine, 16.0, 0.0+4.0*i,0.0);
						break;
			case 2:	QueueDrawSprite(hud_kul, 16.0, 0.0+4.0*i,0.0);
						break;
			case 3:	QueueDrawSprite(hud_sniper, 16.0, 0.0+4.0*i,0.0);
						break;
		}
		pd = (double)g->Lilanek[i].Ammo[g->Lilanek[i].ActiveWeapon]/(double)g->WeaponTemplate[g->Lilanek[i].ActiveWeapon].MaxAmmo;
		QueueDrawSpriteColorizeStretch(hud_ammo, 18.0-2.0*pd, 0.0+4.0*i,4.0*pd, 4.0, 2.0,c);
		pd = (double)g->Lilanek[i].ReloadTime/(double)g->WeaponTemplate[g->Lilanek[i].ActiveWeapon].ReloadTime;
		if(pd>1.0)pd=1.0;
		QueueDrawSpriteColorizeStretch(hud_reload, 18.0-2.0*pd, 0.0+4.0*i,4.0*pd, 4.0, 2.0,c);
		snprintf (fragsbuf, 16, "F: %d", g->Lilanek[i].Frags);
		QueueDrawTextColorize (fragsbuf, 18.1, 4.0*i+0.5, 4.0, InvertColor(g->Lilanek[i].Color));
		snprintf (fragsbuf, 16, "D: %d", g->Lilanek[i].Deaths);
		QueueDrawTextColorize (fragsbuf, 18.1, 4.0*i+0.5*2.0, 4.0, InvertColor(g->Lilanek[i].Color));
	};
	//Weapons
	for(i=0; i < g->noWeapons; i++)
		if(g->Weapon[i].Exists)
		switch(g->Weapon[i].WID){
			case 0:	QueueDrawSprite(brok, g->Weapon[i].Pos.x - 1.0, g->Weapon[i].Pos.y - 1.0,1.0);
						break;
			case 1:	QueueDrawSprite(mines, g->Weapon[i].Pos.x - 1.0, g->Weapon[i].Pos.y - 1.0,1.0);
						break;
			case 2:	QueueDrawSprite(kul, g->Weapon[i].Pos.x - 1.0, g->Weapon[i].Pos.y - 1.0,1.0);
						break;
			case 3:	QueueDrawSprite(sniper, g->Weapon[i].Pos.x - 1.0, g->Weapon[i].Pos.y - 1.0,1.0);
						break;
		}

	for(i=0; i < g->noBullets; i++)
		if(g->Bullet[i].Exists)
		switch(g->Bullet[i].WID){
			case 0: QueueDrawSprite(kulka, g->Bullet[i].Pos.x - 1.0, g->Bullet[i].Pos.y - 1.0,1.0);break;
			case 1: QueueDrawSprite(mine, g->Bullet[i].Pos.x - 1.0, g->Bullet[i].Pos.y - 1.0,1.0);break;
			case 2: QueueDrawSprite(kulka, g->Bullet[i].Pos.x - 1.0, g->Bullet[i].Pos.y - 1.0,1.0);break;
			case -1: QueueDrawSprite(explo, g->Bullet[i].Pos.x - 1.0, g->Bullet[i].Pos.y - 1.0,1.0);break;
		}
	
}

int InitAll(Game **g){
	Config conf;
	conf = GetConfig();
	SaveConfig();
	//TODO: odstranit SaveConfig... je to tu jenom kvuli debugovani


	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER |SDL_INIT_AUDIO ) < 0) {
		fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
		return 1;
	}
	atexit(onExit);
	
	InitBase();
	GrInit();
	SoundInit();
	FontInit();
	*g = testgame();
	printf(PATH_GRAPHICS "lil.svg");
	lil[0] = LoadSpriteSVG(PATH_GRAPHICS "lil.svg", 1.0, 1.0);
	lil[1] = LoadSpriteSVG(PATH_GRAPHICS "lilb.svg", 1.0, 1.0);
	lil[2] = LoadSpriteSVG(PATH_GRAPHICS "lill.svg", 1.0, 1.0);
	lil[3] = LoadSpriteSVG(PATH_GRAPHICS "lil.svg", 1.0, 1.0);
	lil[4] = LoadSpriteSVG(PATH_GRAPHICS "lilr.svg", 1.0, 1.0);
	(*g)->Map->Sprites[0] = LoadSpriteSVG(PATH_GRAPHICS "prek1.svg", 3.0, 3.0);
	(*g)->Map->Sprites[1] = LoadSpriteSVG(PATH_GRAPHICS "prek2.svg", 4.0, 4.0);
	(*g)->Map->Sprites[0]->z = 1;
	(*g)->Map->Sprites[1]->z = 1;
	bg=0;
	bg = LoadSpriteSVG(PATH_GRAPHICS "bg.svg", 16.0, 15.0);
	kul = LoadSpriteSVG(PATH_GRAPHICS "kul.svg", 0.5, 0.5);
	explo = LoadSpriteSVG(PATH_GRAPHICS "exp.svg", 0.5, 0.5);
	dust = LoadSpriteSVG(PATH_GRAPHICS "dust.svg", 0.5, 0.5);
	blood = LoadSpriteSVG(PATH_GRAPHICS "blood.svg", 0.5, 0.5);
	brok = LoadSpriteSVG(PATH_GRAPHICS "brok.svg", 0.5, 0.5);
	sniper = LoadSpriteSVG(PATH_GRAPHICS "sniper.svg", 0.5, 0.5);
	kulka = LoadSpriteSVG(PATH_GRAPHICS "kulka.svg", 0.1, 0.1);
	mine = LoadSpriteSVG(PATH_GRAPHICS "mine.svg", 0.5, 0.5);
	mines = LoadSpriteSVG(PATH_GRAPHICS "mines.svg", 0.5, 0.5);
	hud = LoadSpriteSVG(PATH_GRAPHICS "hud.svg", 4.0, 4.0);
	hud_mine = LoadSpriteSVG(PATH_GRAPHICS "mines.svg", 2.2, 2.2);
	hud_kul = LoadSpriteSVG(PATH_GRAPHICS "kul.svg", 2.2, 2.2);
	hud_brok = LoadSpriteSVG(PATH_GRAPHICS "brok.svg", 2.2, 2.2);
	hud_sniper = LoadSpriteSVG(PATH_GRAPHICS "sniper.svg", 2.2, 2.2);
	hud_ammo = LoadSpriteSVG(PATH_GRAPHICS "hud_ammo.svg", 4.0, 4.0);
	hud_reload = LoadSpriteSVG(PATH_GRAPHICS "hud_reload.svg", 4.0, 4.0);

	s_die = LoadSound(PATH_SOUNDS "die.wav");
	s_bonus = LoadSound(PATH_SOUNDS "bonus.wav");
	s_expl = LoadSound(PATH_SOUNDS "expl.wav");
	s_shoot = LoadSound(PATH_SOUNDS "shoot.wav");
	s_mine = LoadSound(PATH_SOUNDS "mine.wav");

	SDL_AddTimer(1000, callback_fps, NULL);
	
	return 0;
}

int GameLoop(Game *g){
	SDL_Event event;
	Config conf;
	double lasttime, dtime;

	lasttime = SDL_GetTicks()*0.001;
	while (1) {
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_KEYDOWN:
				if (event.key.keysym.sym == SDLK_q) {
					return 0;
				}
				break;

			case SDL_QUIT:
				return 0;
				break;

			case SDL_VIDEORESIZE:
				/* Resize the screen and redraw */
				conf=GetConfig();
				conf.screen_width=event.resize.w;
				conf.screen_height=event.resize.h;
				SetConfig(conf);

				InvalidateSprites();
				break;

			case SDL_USEREVENT:
				printf("%d frames\n",frames);
				frames=0;
				break;

			default:
			break;
			}
		}
		dtime = SDL_GetTicks()*0.001-lasttime;
		lasttime += dtime;
		if(dtime < 0)
			dtime=0.0;
		if(dtime > 0.1)
			dtime=0.1;
		UpdateLogic(g, dtime*0.5);
		UpdateLogic(g, dtime*0.5);
		ClrScr();
		Draw(g);
		UpdateAndQueueDrawParticles(dtime);
		DrawSprites();
		frames++;
		//we don't want to waste cpu...
		if(frames >=50){
			SDL_Delay(10);
		}
	}
	return 0;
}

int main(int argc, char **argv){
	Game *g;
	srand(time(0));
	InitAll(&g);
	GameLoop(g);
	return 0;
}
