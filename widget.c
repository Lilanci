#include "widget.h"
#include <string.h>


int HandleEvent(TWidget *widget, char event[EVENT_LENGHT], void *data){
	TEventHandler *eh;

	eh = widget->handlers;
	while(eh != 0){
		if(strncmp(event, eh->event, EVENT_LENGHT) == 0)
			return eh->handler(widget, data, eh->userdata);
		eh = eh->next;
	};

	return 0;
}

void AddHandler(TWidget *widget char event[EVENT_LENGHT], int (*handler)(TWidget *widget, void *data, void *userdata), void *userdata){
	TEventHandler *eh;

	eh = malloc(sizeof(TEventHandler));
	memset(eh, 0, sizeof(TEventHandler)); //TODO:check if this is valid for eh->event
	strncpy(eh->event, event, EVENT_LENGHT);
	eh->userdata = userdata;
	eh->next = widget->handlers;
	eh->handler = handler;
	widget->handlers = eh;
}
