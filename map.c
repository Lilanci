#include "map.h"

int AreaInMap(TMap *m, Area *p, int FromLayer, int ToLayer){ //returns 0 if p collides with something in m otherwise 1
	int i,j;
	if(!AreaInAreaComp(m->BoundingArea, p)){
		return 0;
	}
	for(i = FromLayer; i < m->noLayer && i <= ToLayer; i++){	
		for(j=0; j < m->Layer[i]->noFArea; j++)
			if(AreaInArea(p, m->Layer[i]->FArea[j]))
				return 0;
	}
	return 1;
}

//TODO: support for another architectures than 32-bit x86
typedef unsigned int uint32;
typedef int int32;
typedef double double64;

typedef struct {
	double64 XX;
	double64 YY;
	double64 X;
	double64 Y;
	uint32 noSprites;
	uint32 noLayer;
	uint32 *Layer_seek; //[noLayer]
	uint32 *Sprite_seek; //[noSprite]
	uint32 size;
}TMapFileHeader;

typedef struct{
	uint32 noFArea;
	uint32 *string_seek; //noFArea
	uint32 size;
}TMapFileLayer;

typedef struct{
	double64 x;
	double64 y;
	uint32 filename_seek;
	uint32 size;
}TMapFileSprite;

typedef struct{
	void *buffer;
	TMapFileHeader header;
	TMapFileLayer *layers;
	TMapFileSprite *sprites;
	uint32 buffer_lenght;
}TMapFileHelper;

uint32 AddStringToFile(TMapFileHelper *h, char *s) {
	uint32 strl = strlen(s);
	h->buffer = realloc(h->buffer, h->buffer_lenght + strl*sizeof(char) + 1);
	memcpy(h->buffer+h->buffer_lenght, s, strl*sizeof(char) + 1);
	h->buffer_lenght += strl*sizeof(char) + 1;

	return h->buffer_lenght - strl*sizeof(char) + 1;
}


//TODO:
void FillTMapFileHeader(TMapFileHeader *header, TMap *m) {
}
char *TMapFileHeader2String (TMapFileHeader *header) {
}

char *Map2String(TMap *m, size_t *bytes) {
	TMapFileHelper h;
	char *buffer;

	memset(&h, 0, sizeof(TMapFileHelper));

	FillTMapFileHeader(&h.header, m); //warning, Layer_seek&Sprite_seek are only allocated, not filled
	buffer = TMapFileHeader2String (&h.header);
	AddStringToFile(&h, buffer);
	free (buffer);

	//TODO:

	*bytes = h.buffer_lenght;
	return (char *)(h.buffer);
};

