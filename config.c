#include "config.h"
#include <stdio.h>
#include <SDL/SDL_keysym.h>

Config conf;
int loaded=0;

void DefaultConfig();

Config GetConfig(){
	int e;
	if(!loaded){
		if((e=LoadConfig())){
			printf("Config error:%d\n",e);
			DefaultConfig();
			printf("Writing default config\n");
			SaveConfig();
		}
		loaded=1;
	}
	return conf;
} 

void SetConfig(const Config c){
	conf=c;
}

//return values
//0 -O.K.
//1 -conf.txt doesn't exists
//2..8 missing values
int LoadConfig(){
	FILE *fp;

	fp=(FILE*)fopen(CONFIG_FILENAME, "r");
	if(fp==0)
		return 1;
	if(fscanf(fp,"%u", &conf.screen_width)==EOF)return 2;
	if(fscanf(fp,"%u", &conf.screen_height)==EOF)return 3;
	if(fscanf(fp,"%u", &conf.fullscreen)==EOF)return 3;
	if(fscanf(fp,"%u", &conf.texture_lod)==EOF)return 3;
	
	if(fscanf(fp,"%u", &conf.pl1_key_up)==EOF)return 4;
	if(fscanf(fp,"%u", &conf.pl1_key_down)==EOF)return 5;
	if(fscanf(fp,"%u", &conf.pl1_key_left)==EOF)return 6;
	if(fscanf(fp,"%u", &conf.pl1_key_right)==EOF)return 7;
	if(fscanf(fp,"%u", &conf.pl1_key_fire)==EOF)return 8;
	if(fscanf(fp,"%u", &conf.pl1_key_suicide)==EOF)return 8;
	if(fscanf(fp,"%u", &conf.pl1_key_switch)==EOF)return 8;

	if(fscanf(fp,"%u", &conf.pl2_key_up)==EOF)return 4;
	if(fscanf(fp,"%u", &conf.pl2_key_down)==EOF)return 5;
	if(fscanf(fp,"%u", &conf.pl2_key_left)==EOF)return 6;
	if(fscanf(fp,"%u", &conf.pl2_key_right)==EOF)return 7;
	if(fscanf(fp,"%u", &conf.pl2_key_fire)==EOF)return 8;
	if(fscanf(fp,"%u", &conf.pl2_key_suicide)==EOF)return 8;
	if(fscanf(fp,"%u", &conf.pl2_key_switch)==EOF)return 8;
	fclose(fp);
	
	return 0;
}

//0-O.K.
//1-cannot open for writing
int SaveConfig(){
	FILE *fp;

	fp=(FILE*)fopen(CONFIG_FILENAME, "w");
	if(fp==0)
		return 1;
	if(fprintf(fp,"%u\n", conf.screen_width)<0)return 2;
	if(fprintf(fp,"%u\n", conf.screen_height)<0)return 3;
	if(fprintf(fp,"%u\n", conf.fullscreen)<0)return 3;
	if(fprintf(fp,"%u\n", conf.texture_lod)<0)return 3;
	
	if(fprintf(fp,"%u\n", conf.pl1_key_up)<0)return 4;
	if(fprintf(fp,"%u\n", conf.pl1_key_down)<0)return 5;
	if(fprintf(fp,"%u\n", conf.pl1_key_left)<0)return 6;
	if(fprintf(fp,"%u\n", conf.pl1_key_right)<0)return 7;
	if(fprintf(fp,"%u\n", conf.pl1_key_fire)<0)return 8;
	if(fprintf(fp,"%u\n", conf.pl1_key_suicide)<0)return 8;
	if(fprintf(fp,"%u\n", conf.pl1_key_switch)<0)return 8;
	
	if(fprintf(fp,"%u\n", conf.pl2_key_up)<0)return 4;
	if(fprintf(fp,"%u\n", conf.pl2_key_down)<0)return 5;
	if(fprintf(fp,"%u\n", conf.pl2_key_left)<0)return 6;
	if(fprintf(fp,"%u\n", conf.pl2_key_right)<0)return 7;
	if(fprintf(fp,"%u\n", conf.pl2_key_fire)<0)return 8;
	if(fprintf(fp,"%u\n", conf.pl2_key_suicide)<0)return 8;
	if(fprintf(fp,"%u\n", conf.pl2_key_switch)<0)return 8;
	fclose(fp);
	
	return 0;
}

void DefaultConfig(){
	conf.screen_width=800;
	conf.screen_height=600;
	conf.fullscreen=0;
	conf.texture_lod=0;
	
	conf.pl1_key_up=SDLK_UP;
	conf.pl1_key_down=SDLK_DOWN;
	conf.pl1_key_left=SDLK_LEFT;
	conf.pl1_key_right=SDLK_RIGHT;
	conf.pl1_key_fire=SDLK_RCTRL;
	conf.pl1_key_suicide=SDLK_F12;
	conf.pl1_key_switch=SDLK_RSHIFT;

	conf.pl2_key_up=SDLK_w;
	conf.pl2_key_down=SDLK_s;
	conf.pl2_key_left=SDLK_a;
	conf.pl2_key_right=SDLK_d;
	conf.pl2_key_fire=SDLK_LCTRL;
	conf.pl2_key_suicide=SDLK_F11;
	conf.pl2_key_switch=SDLK_LSHIFT;
}
