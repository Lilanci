#ifndef __WIDGET_H_INCLUDED
#define __WIDGET_H_INCLUDED
#define EVENT_LENGTH 8


typedef struct _TEventHandler{
	char event[EVENT_LENGHT];
	void *userdata;
	int (*handler)(struct _TWidget *widget, void *data, void *userdata);
	struct _TEventHandler *next;
}TEventHandler;

typedef struct _TWidget{
	TEventHandler *handlers; //linked list of TEventHandler terminated by ->next==0
	void *data;
}TWidget;

int HandleEvent(TWidget *widget, char event[EVENT_LENGHT], void *data);
void AddHandler(TWidget *widgetm char event[EVENT_LENGHT], int (*handler)(TWidget *widget, void *data, void *userdata), void *userdata);
#endif
