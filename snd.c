#include "snd.h"
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
	Mix_Chunk *sound;
	char *filename;
}TSound;

typedef struct{
	TSound *sounds;
	int allocated;
	int no_sounds;
}TSoundLibrary;

TSoundLibrary g_snd_lib;

int RegenerateSounds(){
	printf("RegenerateSounds not implemented\n");
	return 0;
};

void InvalidateSounds(){
	printf("InvalidateSounds not implemented\n");
};

void PlaySound(int id){
	int channel;
	if(id<0 || id>=g_snd_lib.no_sounds) return;
	channel = Mix_PlayChannel(id, g_snd_lib.sounds[id].sound, 0);
	if(channel == -1) {
		fprintf(stderr, "Unable to play WAV file: %s\n", Mix_GetError());
	}
	printf("Play sound %d\n",id);
};

int LoadSound(char *path){
	int i;
	if((i=(++g_snd_lib.no_sounds))>g_snd_lib.allocated){
		g_snd_lib.allocated *=2;
		g_snd_lib.allocated +=1;
		g_snd_lib.sounds = realloc(g_snd_lib.sounds, sizeof(TSound)* g_snd_lib.allocated);
	};
	i--;
	g_snd_lib.sounds[i].filename = calloc(strlen(path)+1,sizeof(char));
	strcpy(g_snd_lib.sounds[i].filename, path);
	g_snd_lib.sounds[i].sound = Mix_LoadWAV(path);
	if(g_snd_lib.sounds[i].sound == NULL) {
		fprintf(stderr, "Unable to load WAV file: %s\n", Mix_GetError());
	}

	return i;
};

void DestroySound(int id){
	printf("DestroySound not implemented\n");
};

void SoundInit(){
	int audio_rate = 22050;
	Uint16 audio_format = AUDIO_S16SYS;
	int audio_channels = 2;
	int audio_buffers = 4096;

	if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) != 0) {
		fprintf(stderr, "Unable to initialize audio: %s\n", Mix_GetError());
		exit(1);
	}

	memset(&g_snd_lib, 0, sizeof(TSoundLibrary));
};
void SoundKill(){
	Mix_CloseAudio();
};

void SoundDestroyAll(){
	printf("SoundDestroyAll not implemented\n");
};
